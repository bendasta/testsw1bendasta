import engine.Board.Board;
import engine.Board.BoardBuilder;
import engine.Board.Move;
import engine.Board.Tile;
import engine.Color;
import engine.Pieces.King;
import engine.Pieces.Knight;
import engine.Pieces.Piece;
import engine.Pieces.PiecesStringType;
import engine.Player.BlackPlayer;
import engine.Player.MoveTransition;
import engine.Player.WhitePlayer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class BlackInCheck {
    Board gameBoard;
    Knight initBlackKnight;
    BlackPlayer blackPlayer;
    WhitePlayer whitePlayer;
    Collection<Piece> blackPieces;
    Collection<Piece> whitePieces;
    Collection<Move> whiteNormalMoves;
    Collection<Move> blackNormalMoves;
    List<Tile> gamerBoard;

    @Test
    public void whiteInCheck_BlackPieceWillMove_WhiteKingWillGetInCheck() {
        gameBoard = createBoardForTest();
        gamerBoard = Board.crateBoard(new BoardBuilder());


        blackPieces = AllActivePieces(gamerBoard, Color.BLACK);
        whitePieces = AllActivePieces(gamerBoard,Color.WHITE);
        whiteNormalMoves = gameBoard.allLegalMoves(whitePieces);
        blackNormalMoves = gameBoard.allLegalMoves(blackPieces);
        blackPlayer = new BlackPlayer(gameBoard,blackNormalMoves,whiteNormalMoves);
        whitePlayer = new WhitePlayer(gameBoard,whiteNormalMoves,blackNormalMoves);

        King initWhiteKing = (King) gameBoard.getTile(60).getPiece();
        King initBlackKing = (King) gameBoard.getTile(4).getPiece();
        initBlackKnight = (Knight) gameBoard.getTile(25).getPiece();

        //white king
        Assertions.assertEquals(initWhiteKing.getPiecesStringType(), PiecesStringType.KING);
        //black king
        Assertions.assertEquals(initBlackKing.getPiecesStringType(),PiecesStringType.KING);

        //black knight
        Assertions.assertEquals(initBlackKnight.getPiecesStringType(), PiecesStringType.KNIGHT);

        ArrayList<String> knightAllowedMoves = new ArrayList<>();
        for (Move move : initBlackKnight.calculateAllLegalMoves(gameBoard)){
            knightAllowedMoves.add(move.toString());
        }
        ArrayList<String> expectedKnightAllowedMoves = new ArrayList<>(new ArrayList<>(Arrays.asList("a7","c7","d6","d4","a3","c3")));
        Assertions.assertEquals(expectedKnightAllowedMoves,knightAllowedMoves);

        Move move = Move.MoveCreation.createMove(gameBoard,25,19);
        MoveTransition transition = gameBoard.currentPlayer().makeMove(move);
        gameBoard = transition.getTransitionBoard();

        Piece emptyCurrentPosition = gameBoard.getTile(25).getPiece();
        Assertions.assertNull(emptyCurrentPosition);
        initBlackKnight = (Knight) gameBoard.getTile(19).getPiece();
        blackPieces = AllActivePieces(gamerBoard,Color.BLACK);
        whitePieces = AllActivePieces(gamerBoard,Color.WHITE);
        whiteNormalMoves = gameBoard.allLegalMoves(whitePieces);
        blackNormalMoves = gameBoard.allLegalMoves(blackPieces);
        blackPlayer = new BlackPlayer(gameBoard,blackNormalMoves,whiteNormalMoves);
        whitePlayer = new WhitePlayer(gameBoard,whiteNormalMoves,blackNormalMoves);

        int newKnightPosition = initBlackKnight.getPosition();
        int newExpectedKnightPosition = 19;
        Assertions.assertEquals(newExpectedKnightPosition, newKnightPosition);

        ArrayList<String> newKnightAllowedMoves = new ArrayList<>();
        for (Move newMove : initBlackKnight.calculateAllLegalMoves(gameBoard)){
            newKnightAllowedMoves.add(newMove.toString());
        }

        ArrayList<String> newExpectedKnightAllowedMoves = new ArrayList<>(Arrays.asList("c8","dxe8","b7","f7","b5","f5","c4","e4"));
        Assertions.assertEquals(newExpectedKnightAllowedMoves,newKnightAllowedMoves);

        boolean isWhiteInCheck = gameBoard.currentPlayer().getOpponent().isInCheck();
        boolean isBlackInCheck = gameBoard.currentPlayer().isInCheck();

        Assertions.assertFalse(isWhiteInCheck);
        Assertions.assertTrue(isBlackInCheck);
    }

    public static Board createBoardForTest(){
        BoardBuilder builder = new BoardBuilder();
        //setting black pieces
        builder.setPiece(new King(60,Color.WHITE));
        builder.setPiece(new Knight(25,Color.WHITE));

        //setting white pieces
        builder.setPiece(new King(4,Color.BLACK));

        //setting who have the first move
        builder.setColorNextMove(Color.WHITE);

        return builder.buildBoard();
    }

    private Collection<Piece> AllActivePieces(List<Tile> gamerBoard, Color color) {
        final List<Piece> activePieces = new ArrayList<>();
        for (final Tile tile : gamerBoard){
            if (tile.isTileOccupied()){
                final Piece piece = tile.getPiece();
                if (piece.getPieceColor() == color){
                    activePieces.add(piece);
                }
            }
        }
        return activePieces;
    }
}
