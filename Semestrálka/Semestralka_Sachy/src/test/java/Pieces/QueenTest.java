package Pieces;

import engine.Board.Board;
import engine.Board.BoardBuilder;
import engine.Board.Move;
import engine.Color;
import engine.Pieces.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.mockito.Mockito.*;

public class QueenTest {
    Board gameBoard;
    BoardBuilder builder = new BoardBuilder();

    @Test
    public void CheckPossibleMoves_WhiteQueenMovesFromD1_WhiteQueenHas6LegalMoves() {
        gameBoard = createBoardForWhiteTest();
        ArrayList<String> result = new ArrayList<>();
        for(Move move : gameBoard.getTile(59).getPiece().calculateAllLegalMoves(gameBoard)){
            result.add(move.toString());
        }
        Piece tested = gameBoard.getTile(59).getPiece();

        Queen mockQueen = mock(Queen.class);
        when(mockQueen.calculateAllLegalMoves(gameBoard)).thenReturn(new ArrayList<>(){{add(new Move.NormalMove(gameBoard,51,tested));
            add(new Move.NormalMove(gameBoard,43,tested));
            add(new Move.NormalMove(gameBoard,35,tested));add(new Move.NormalMove(gameBoard,27,tested));
            add(new Move.NormalMove(gameBoard,19,tested));add(new Move.AttackMove(gameBoard,11,tested,gameBoard.getTile(11).getPiece()));}});

        ArrayList<String> expectedResult = new ArrayList<>();
        for (Move move : mockQueen.calculateAllLegalMoves(gameBoard)){
            expectedResult.add(move.toString());
        }
        verify(mockQueen).calculateAllLegalMoves(gameBoard);
        Assertions.assertEquals(expectedResult,result);
    }

    @Test
    public void CheckPossibleMoves_BlackQueenMovesFromD8_BlackQueenHas6LegalMoves() {
        gameBoard = createBoardForBlackTest();
        ArrayList<String> result = new ArrayList<>();
        for(Move move : gameBoard.getTile(3).getPiece().calculateAllLegalMoves(gameBoard)){
            result.add(move.toString());
        }
        Piece tested = gameBoard.getTile(3).getPiece();

        Bishop mockQueen = mock(Bishop.class);
        when(mockQueen.calculateAllLegalMoves(gameBoard)).thenReturn(new ArrayList<>(){{add(new Move.NormalMove(gameBoard,11,tested));
            add(new Move.NormalMove(gameBoard,19,tested));
            add(new Move.NormalMove(gameBoard,27,tested));add(new Move.NormalMove(gameBoard,35,tested));
            add(new Move.NormalMove(gameBoard,43,tested));add(new Move.AttackMove(gameBoard,51,tested,gameBoard.getTile(51).getPiece()));}});

        ArrayList<String> expectedResult = new ArrayList<>();
        for (Move move : mockQueen.calculateAllLegalMoves(gameBoard)){
            expectedResult.add(move.toString());
        }
        verify(mockQueen).calculateAllLegalMoves(gameBoard);
        Assertions.assertEquals(result,expectedResult);
    }

    public static Board createBoardForWhiteTest(){
        //setting black pieces
        BoardBuilder builder = new BoardBuilder();
        builder.setPiece(new Rook(0, Color.BLACK));
        builder.setPiece(new Knight(1,Color.BLACK));
        builder.setPiece(new Bishop(2,Color.BLACK));
        builder.setPiece(new Queen(3,Color.BLACK));
        builder.setPiece(new King(4,Color.BLACK));
        builder.setPiece(new Bishop(5,Color.BLACK));
        builder.setPiece(new Knight(6,Color.BLACK));
        builder.setPiece(new Rook(7,Color.BLACK));
        builder.setPiece(new Pawn(8,Color.BLACK));
        builder.setPiece(new Pawn(9,Color.BLACK));
        builder.setPiece(new Pawn(10,Color.BLACK));
        builder.setPiece(new Pawn(11,Color.BLACK));
        builder.setPiece(new Pawn(12,Color.BLACK));
        builder.setPiece(new Pawn(13,Color.BLACK));
        builder.setPiece(new Pawn(14,Color.BLACK));
        builder.setPiece(new Pawn(15,Color.BLACK));

        //setting white pieces
        builder.setPiece(new Rook(63,Color.WHITE));
        builder.setPiece(new Knight(62,Color.WHITE));
        builder.setPiece(new Bishop(61,Color.WHITE));
        builder.setPiece(new King(60,Color.WHITE));
        builder.setPiece(new Queen(59,Color.WHITE));
        builder.setPiece(new Bishop(58,Color.WHITE));
        builder.setPiece(new Knight(57,Color.WHITE));
        builder.setPiece(new Rook(56,Color.WHITE));
        builder.setPiece(new Pawn(55,Color.WHITE));
        builder.setPiece(new Pawn(54,Color.WHITE));
        builder.setPiece(new Pawn(53,Color.WHITE));
        builder.setPiece(new Pawn(52,Color.WHITE));
        builder.setPiece(new Pawn(42,Color.WHITE));
        builder.setPiece(new Pawn(50,Color.WHITE));
        builder.setPiece(new Pawn(49,Color.WHITE));
        builder.setPiece(new Pawn(48,Color.WHITE));

        //setting who have the first move
        builder.setColorNextMove(Color.WHITE);

        return builder.buildBoard();
    }

    public static Board createBoardForBlackTest(){
        //setting black pieces
        BoardBuilder builder = new BoardBuilder();
        builder.setPiece(new Rook(0, Color.BLACK));
        builder.setPiece(new Knight(1,Color.BLACK));
        builder.setPiece(new Bishop(2,Color.BLACK));
        builder.setPiece(new Queen(3,Color.BLACK));
        builder.setPiece(new King(4,Color.BLACK));
        builder.setPiece(new Bishop(5,Color.BLACK));
        builder.setPiece(new Knight(6,Color.BLACK));
        builder.setPiece(new Rook(7,Color.BLACK));
        builder.setPiece(new Pawn(8,Color.BLACK));
        builder.setPiece(new Pawn(9,Color.BLACK));
        builder.setPiece(new Pawn(10,Color.BLACK));
        builder.setPiece(new Pawn(18,Color.BLACK));
        builder.setPiece(new Pawn(12,Color.BLACK));
        builder.setPiece(new Pawn(13,Color.BLACK));
        builder.setPiece(new Pawn(14,Color.BLACK));
        builder.setPiece(new Pawn(15,Color.BLACK));

        //setting white pieces
        builder.setPiece(new Rook(63,Color.WHITE));
        builder.setPiece(new Knight(62,Color.WHITE));
        builder.setPiece(new Bishop(61,Color.WHITE));
        builder.setPiece(new King(60,Color.WHITE));
        builder.setPiece(new Queen(59,Color.WHITE));
        builder.setPiece(new Bishop(58,Color.WHITE));
        builder.setPiece(new Knight(57,Color.WHITE));
        builder.setPiece(new Rook(56,Color.WHITE));
        builder.setPiece(new Pawn(55,Color.WHITE));
        builder.setPiece(new Pawn(54,Color.WHITE));
        builder.setPiece(new Pawn(53,Color.WHITE));
        builder.setPiece(new Pawn(52,Color.WHITE));
        builder.setPiece(new Pawn(51,Color.WHITE));
        builder.setPiece(new Pawn(50,Color.WHITE));
        builder.setPiece(new Pawn(49,Color.WHITE));
        builder.setPiece(new Pawn(48,Color.WHITE));

        //setting who have the first move
        builder.setColorNextMove(Color.WHITE);

        return builder.buildBoard();
    }
}
