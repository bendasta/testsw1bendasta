package Pieces;

import engine.Board.Board;
import engine.Board.BoardBuilder;
import engine.Board.Move;
import engine.Color;
import engine.Pieces.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

public class RookTest {
    Board gameBoard;
    BoardBuilder builder = new BoardBuilder();
    @Test
    public void CheckPossibleMoves_WhiteRookMovesFromH3_WhiteRookHas10LegalMoves() {
        gameBoard = createBoardForWhiteTest();
        ArrayList<String> result = new ArrayList<>();
        for(Move move : gameBoard.getTile(47).getPiece().calculateAllLegalMoves(gameBoard)){
            result.add(move.toString());
        }
        Piece tested = gameBoard.getTile(47).getPiece();

        Rook mockRook = mock(Rook.class);

        when(mockRook.calculateAllLegalMoves(gameBoard)).thenReturn(new ArrayList<>(){{add(new Move.NormalMove(gameBoard,39,tested));
            add(new Move.NormalMove(gameBoard,31,tested));add(new Move.NormalMove(gameBoard,23,tested));
            add(new Move.AttackMove(gameBoard,15,tested,gameBoard.getTile(15).getPiece()));add(new Move.NormalMove(gameBoard,46,tested));
            add(new Move.NormalMove(gameBoard,45,tested));add(new Move.NormalMove(gameBoard,44,tested));
            add(new Move.NormalMove(gameBoard,43,tested));
            add(new Move.NormalMove(gameBoard,42,tested));add(new Move.NormalMove(gameBoard,41,tested));
            add(new Move.NormalMove(gameBoard,40,tested));}});

        ArrayList<String> expectedResult = new ArrayList<>();
        for (Move move : mockRook.calculateAllLegalMoves(gameBoard)){
            expectedResult.add(move.toString());
        }
        verify(mockRook).calculateAllLegalMoves(gameBoard);
        Assertions.assertEquals(expectedResult, result);

    }

    @Test
    public void CheckPossibleMoves_BlackRookMovesFromA6_BlackRookHas10LegalMoves() {
        gameBoard = createBoardForBlackTest();
        ArrayList<String> result = new ArrayList<>();
        for(Move move : gameBoard.getTile(16).getPiece().calculateAllLegalMoves(gameBoard)){
            result.add(move.toString());
        }
        Piece tested = gameBoard.getTile(16).getPiece();

        Bishop mockRook = mock(Bishop.class);

        when(mockRook.calculateAllLegalMoves(gameBoard)).thenReturn(new ArrayList<>(){{add(new Move.NormalMove(gameBoard,17,tested));
            add(new Move.NormalMove(gameBoard,18,tested));
            add(new Move.NormalMove(gameBoard,19,tested));add(new Move.NormalMove(gameBoard,20,tested));
            add(new Move.NormalMove(gameBoard,21,tested));
            add(new Move.NormalMove(gameBoard,22,tested));add(new Move.NormalMove(gameBoard,23,tested));add(new Move.NormalMove(gameBoard,24,tested));add(new Move.NormalMove(gameBoard,32,tested));
            add(new Move.NormalMove(gameBoard,40,tested));add(new Move.AttackMove(gameBoard,48,tested,gameBoard.getTile(48).getPiece()));}});
        ArrayList<String> expectedResult = new ArrayList<>();
        for (Move move : mockRook.calculateAllLegalMoves(gameBoard)){
            expectedResult.add(move.toString());
        }
        verify(mockRook).calculateAllLegalMoves(gameBoard);
        Assertions.assertEquals(result,expectedResult);
    }

    public static Board createBoardForWhiteTest(){
        //setting black pieces
        BoardBuilder builder = new BoardBuilder();
        builder.setPiece(new Rook(0, Color.BLACK));
        builder.setPiece(new Knight(1,Color.BLACK));
        builder.setPiece(new Bishop(2,Color.BLACK));
        builder.setPiece(new Queen(3,Color.BLACK));
        builder.setPiece(new King(4,Color.BLACK));
        builder.setPiece(new Bishop(5,Color.BLACK));
        builder.setPiece(new Knight(6,Color.BLACK));
        builder.setPiece(new Rook(7,Color.BLACK));
        builder.setPiece(new Pawn(8,Color.BLACK));
        builder.setPiece(new Pawn(9,Color.BLACK));
        builder.setPiece(new Pawn(10,Color.BLACK));
        builder.setPiece(new Pawn(11,Color.BLACK));
        builder.setPiece(new Pawn(12,Color.BLACK));
        builder.setPiece(new Pawn(13,Color.BLACK));
        builder.setPiece(new Pawn(14,Color.BLACK));
        builder.setPiece(new Pawn(15,Color.BLACK));

        //setting white pieces
        builder.setPiece(new Rook(47,Color.WHITE));
        builder.setPiece(new Knight(62,Color.WHITE));
        builder.setPiece(new Bishop(61,Color.WHITE));
        builder.setPiece(new King(60,Color.WHITE));
        builder.setPiece(new Queen(59,Color.WHITE));
        builder.setPiece(new Bishop(58,Color.WHITE));
        builder.setPiece(new Knight(57,Color.WHITE));
        builder.setPiece(new Rook(56,Color.WHITE));
        builder.setPiece(new Pawn(55,Color.WHITE));
        builder.setPiece(new Pawn(54,Color.WHITE));
        builder.setPiece(new Pawn(53,Color.WHITE));
        builder.setPiece(new Pawn(52,Color.WHITE));
        builder.setPiece(new Pawn(51,Color.WHITE));
        builder.setPiece(new Pawn(50,Color.WHITE));
        builder.setPiece(new Pawn(49,Color.WHITE));
        builder.setPiece(new Pawn(48,Color.WHITE));

        //setting who have the first move
        builder.setColorNextMove(Color.WHITE);

        return builder.buildBoard();
    }

    /***
     * Board for testing
     * @return board for testing
     */
    public static Board createBoardForBlackTest(){
        //setting black pieces
        BoardBuilder builder = new BoardBuilder();
        builder.setPiece(new Rook(16, Color.BLACK));
        builder.setPiece(new Knight(1,Color.BLACK));
        builder.setPiece(new Bishop(2,Color.BLACK));
        builder.setPiece(new Queen(3,Color.BLACK));
        builder.setPiece(new King(4,Color.BLACK));
        builder.setPiece(new Bishop(5,Color.BLACK));
        builder.setPiece(new Knight(6,Color.BLACK));
        builder.setPiece(new Rook(7,Color.BLACK));
        builder.setPiece(new Pawn(8,Color.BLACK));
        builder.setPiece(new Pawn(9,Color.BLACK));
        builder.setPiece(new Pawn(10,Color.BLACK));
        builder.setPiece(new Pawn(11,Color.BLACK));
        builder.setPiece(new Pawn(12,Color.BLACK));
        builder.setPiece(new Pawn(13,Color.BLACK));
        builder.setPiece(new Pawn(14,Color.BLACK));
        builder.setPiece(new Pawn(15,Color.BLACK));

        //setting white pieces
        builder.setPiece(new Rook(63,Color.WHITE));
        builder.setPiece(new Knight(62,Color.WHITE));
        builder.setPiece(new Bishop(61,Color.WHITE));
        builder.setPiece(new King(60,Color.WHITE));
        builder.setPiece(new Queen(59,Color.WHITE));
        builder.setPiece(new Bishop(58,Color.WHITE));
        builder.setPiece(new Knight(57,Color.WHITE));
        builder.setPiece(new Rook(56,Color.WHITE));
        builder.setPiece(new Pawn(55,Color.WHITE));
        builder.setPiece(new Pawn(54,Color.WHITE));
        builder.setPiece(new Pawn(53,Color.WHITE));
        builder.setPiece(new Pawn(52,Color.WHITE));
        builder.setPiece(new Pawn(51,Color.WHITE));
        builder.setPiece(new Pawn(50,Color.WHITE));
        builder.setPiece(new Pawn(49,Color.WHITE));
        builder.setPiece(new Pawn(48,Color.WHITE));

        //setting who have the first move
        builder.setColorNextMove(Color.WHITE);

        return builder.buildBoard();
    }
}
