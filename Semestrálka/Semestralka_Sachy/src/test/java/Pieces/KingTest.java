package Pieces;

import engine.Board.Board;
import engine.Board.BoardBuilder;
import engine.Board.Move;
import engine.Color;
import engine.Pieces.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

/***
 * Class for King tests
 */
public class KingTest {
    Board gameBoard;
    BoardBuilder builder = new BoardBuilder();
    @Test
    public void CheckPossibleMoves_WhiteKingMovesFromE1_WhiteKingHas2LegalMovesOnCurrentBoard() {
        gameBoard = createBoardForWhiteTest();
        ArrayList<String> result = new ArrayList<>();
        for(Move move : gameBoard.getTile(60).getPiece().calculateAllLegalMoves(gameBoard)){
            result.add(move.toString());
        }
        Piece tested = gameBoard.getTile(60).getPiece();
        King mockKing = mock(King.class);

        when(mockKing.calculateAllLegalMoves(gameBoard)).thenReturn(new ArrayList<>(){{
            add(new Move.KingSideCastleMove(gameBoard,62,tested,(Rook) gameBoard.getTile(63).getPiece(),63,61));
            add(new Move.NormalMove(gameBoard,61,tested));}});

        ArrayList<String> expectedResult = new ArrayList<>();
        for (Move move : mockKing.calculateAllLegalMoves(gameBoard)){
            expectedResult.add(move.toString());
        }
        verify(mockKing).calculateAllLegalMoves(gameBoard);
        Assertions.assertEquals(expectedResult,result);
    }

    @Test
    public void CheckPossibleMoves_BlackKingMovesFromE8_BlackKingHas2LegalMovesOnCurrentBoard() {
        gameBoard = createBoardForBlackTest();
        ArrayList<String> result = new ArrayList<>();
        for(Move move : gameBoard.getTile(4).getPiece().calculateAllLegalMoves(gameBoard)){
            result.add(move.toString());
        }
        Piece tested = gameBoard.getTile(4).getPiece();
        Bishop mockBishop = mock(Bishop.class);

        when(mockBishop.calculateAllLegalMoves(gameBoard)).thenReturn(new ArrayList<>(){{
            add(new Move.KingSideCastleMove(gameBoard,6,tested,(Rook) gameBoard.getTile(7).getPiece(),7,5));
            add(new Move.NormalMove(gameBoard,5,tested));}});

        ArrayList<String> expectedResult = new ArrayList<>();
        for (Move move : mockBishop.calculateAllLegalMoves(gameBoard)){
            expectedResult.add(move.toString());
        }
        verify(mockBishop).calculateAllLegalMoves(gameBoard);
        Assertions.assertEquals(result,expectedResult);
    }

    public static Board createBoardForWhiteTest(){
        //setting black pieces
        BoardBuilder builder = new BoardBuilder();
        builder.setPiece(new Rook(0, Color.BLACK));
        builder.setPiece(new Knight(1,Color.BLACK));
        builder.setPiece(new Bishop(2,Color.BLACK));
        builder.setPiece(new Queen(3,Color.BLACK));
        builder.setPiece(new King(4,Color.BLACK));
        builder.setPiece(new Bishop(5,Color.BLACK));
        builder.setPiece(new Knight(6,Color.BLACK));
        builder.setPiece(new Rook(7,Color.BLACK));
        builder.setPiece(new Pawn(8,Color.BLACK));
        builder.setPiece(new Pawn(9,Color.BLACK));
        builder.setPiece(new Pawn(10,Color.BLACK));
        builder.setPiece(new Pawn(11,Color.BLACK));
        builder.setPiece(new Pawn(12,Color.BLACK));
        builder.setPiece(new Pawn(13,Color.BLACK));
        builder.setPiece(new Pawn(14,Color.BLACK));
        builder.setPiece(new Pawn(15,Color.BLACK));

        //setting white pieces
        builder.setPiece(new Rook(63,Color.WHITE));
        builder.setPiece(new Knight(46,Color.WHITE));
        builder.setPiece(new Bishop(45,Color.WHITE));
        builder.setPiece(new King(60,Color.WHITE));
        builder.setPiece(new Queen(59,Color.WHITE));
        builder.setPiece(new Bishop(58,Color.WHITE));
        builder.setPiece(new Knight(57,Color.WHITE));
        builder.setPiece(new Rook(56,Color.WHITE));
        builder.setPiece(new Pawn(55,Color.WHITE));
        builder.setPiece(new Pawn(54,Color.WHITE));
        builder.setPiece(new Pawn(53,Color.WHITE));
        builder.setPiece(new Pawn(52,Color.WHITE));
        builder.setPiece(new Pawn(51,Color.WHITE));
        builder.setPiece(new Pawn(50,Color.WHITE));
        builder.setPiece(new Pawn(49,Color.WHITE));
        builder.setPiece(new Pawn(48,Color.WHITE));

        //setting who have the first move
        builder.setColorNextMove(Color.WHITE);

        return builder.buildBoard();
    }

    /***
     * Board for testing
     * @return board for testing
     */
    public static Board createBoardForBlackTest(){
        //setting black pieces
        BoardBuilder builder = new BoardBuilder();
        builder.setPiece(new Rook(0, Color.BLACK));
        builder.setPiece(new Knight(1,Color.BLACK));
        builder.setPiece(new Bishop(2,Color.BLACK));
        builder.setPiece(new Queen(3,Color.BLACK));
        builder.setPiece(new King(4,Color.BLACK));
        builder.setPiece(new Bishop(21,Color.BLACK));
        builder.setPiece(new Knight(22,Color.BLACK));
        builder.setPiece(new Rook(7,Color.BLACK));
        builder.setPiece(new Pawn(8,Color.BLACK));
        builder.setPiece(new Pawn(9,Color.BLACK));
        builder.setPiece(new Pawn(10,Color.BLACK));
        builder.setPiece(new Pawn(11,Color.BLACK));
        builder.setPiece(new Pawn(12,Color.BLACK));
        builder.setPiece(new Pawn(13,Color.BLACK));
        builder.setPiece(new Pawn(14,Color.BLACK));
        builder.setPiece(new Pawn(15,Color.BLACK));

        //setting white pieces
        builder.setPiece(new Rook(63,Color.WHITE));
        builder.setPiece(new Knight(62,Color.WHITE));
        builder.setPiece(new Bishop(61,Color.WHITE));
        builder.setPiece(new King(60,Color.WHITE));
        builder.setPiece(new Queen(59,Color.WHITE));
        builder.setPiece(new Bishop(58,Color.WHITE));
        builder.setPiece(new Knight(57,Color.WHITE));
        builder.setPiece(new Rook(56,Color.WHITE));
        builder.setPiece(new Pawn(55,Color.WHITE));
        builder.setPiece(new Pawn(54,Color.WHITE));
        builder.setPiece(new Pawn(53,Color.WHITE));
        builder.setPiece(new Pawn(52,Color.WHITE));
        builder.setPiece(new Pawn(51,Color.WHITE));
        builder.setPiece(new Pawn(50,Color.WHITE));
        builder.setPiece(new Pawn(49,Color.WHITE));
        builder.setPiece(new Pawn(48,Color.WHITE));

        //setting who have the first move
        builder.setColorNextMove(Color.WHITE);

        return builder.buildBoard();
    }
}
