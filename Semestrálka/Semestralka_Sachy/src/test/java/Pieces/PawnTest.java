package Pieces;

import engine.Board.Board;
import engine.Board.BoardBuilder;
import engine.Board.Move;
import engine.Color;
import engine.Pieces.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

/***
 * Class for Pawn tests
 */
public class PawnTest {
    Board gameBoard;
    BoardBuilder builder = new BoardBuilder();

    @BeforeEach
    public void init(){
        gameBoard = createBoardForTest();
    }
    @Test
    public void CheckPossibleMoves_WhitePawnMovesFromF1_WhitePawnHas5LegalMoves() {
        ArrayList<String> result = new ArrayList<>();
        for(Move move : gameBoard.getTile(48).getPiece().calculateAllLegalMoves(gameBoard)){
            result.add(move.toString());
        }
        Piece tested = gameBoard.getTile(48).getPiece();

        Pawn mockPawn = mock(Pawn.class);

        when(mockPawn.calculateAllLegalMoves(gameBoard)).thenReturn(new ArrayList<>(){{add(new Move.PawnMove(gameBoard,40,tested));
            add(new Move.PawnJump(gameBoard,32,tested));}});
        ArrayList<String> expectedResult = new ArrayList<>();
        for (Move move : mockPawn.calculateAllLegalMoves(gameBoard)){
            expectedResult.add(move.toString());
        }
        verify(mockPawn).calculateAllLegalMoves(gameBoard);
        Assertions.assertEquals(expectedResult,result);
    }
    @Test
    public void CheckPossibleMoves_BlackPawnMovesFromC8_BlackPawnHas5LegalMoves() {
        ArrayList<String> result = new ArrayList<>();
        for(Move move : gameBoard.getTile(8).getPiece().calculateAllLegalMoves(gameBoard)){
            result.add(move.toString());
        }
        Piece tested = gameBoard.getTile(8).getPiece();
        Bishop mockPawn = mock(Bishop.class);
        when(mockPawn.calculateAllLegalMoves(gameBoard)).thenReturn(new ArrayList<>(){{add(new Move.PawnMove(gameBoard,16,tested));
            add(new Move.PawnJump(gameBoard,24,tested));}});
        ArrayList<String> expectedResult = new ArrayList<>();
        for (Move move : mockPawn.calculateAllLegalMoves(gameBoard)){
            expectedResult.add(move.toString());
        }
        verify(mockPawn).calculateAllLegalMoves(gameBoard);
        Assertions.assertEquals(result,expectedResult);
    }

    public static Board createBoardForTest(){
        //setting black pieces
        BoardBuilder builder = new BoardBuilder();
        builder.setPiece(new Rook(0, Color.BLACK));
        builder.setPiece(new Knight(1,Color.BLACK));
        builder.setPiece(new Bishop(2,Color.BLACK));
        builder.setPiece(new Queen(3,Color.BLACK));
        builder.setPiece(new King(4,Color.BLACK));
        builder.setPiece(new Bishop(5,Color.BLACK));
        builder.setPiece(new Knight(6,Color.BLACK));
        builder.setPiece(new Rook(7,Color.BLACK));
        builder.setPiece(new Pawn(8,Color.BLACK));
        builder.setPiece(new Pawn(9,Color.BLACK));
        builder.setPiece(new Pawn(10,Color.BLACK));
        builder.setPiece(new Pawn(11,Color.BLACK));
        builder.setPiece(new Pawn(12,Color.BLACK));
        builder.setPiece(new Pawn(13,Color.BLACK));
        builder.setPiece(new Pawn(14,Color.BLACK));
        builder.setPiece(new Pawn(15,Color.BLACK));

        //setting white pieces
        builder.setPiece(new Rook(63,Color.WHITE));
        builder.setPiece(new Knight(62,Color.WHITE));
        builder.setPiece(new Bishop(61,Color.WHITE));
        builder.setPiece(new King(60,Color.WHITE));
        builder.setPiece(new Queen(59,Color.WHITE));
        builder.setPiece(new Bishop(58,Color.WHITE));
        builder.setPiece(new Knight(57,Color.WHITE));
        builder.setPiece(new Rook(56,Color.WHITE));
        builder.setPiece(new Pawn(55,Color.WHITE));
        builder.setPiece(new Pawn(54,Color.WHITE));
        builder.setPiece(new Pawn(53,Color.WHITE));
        builder.setPiece(new Pawn(52,Color.WHITE));
        builder.setPiece(new Pawn(51,Color.WHITE));
        builder.setPiece(new Pawn(50,Color.WHITE));
        builder.setPiece(new Pawn(49,Color.WHITE));
        builder.setPiece(new Pawn(48,Color.WHITE));

        //setting who have the first move
        builder.setColorNextMove(Color.WHITE);

        return builder.buildBoard();
    }

}
