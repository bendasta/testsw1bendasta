import engine.Board.Board;
import engine.Board.BoardBuilder;
import engine.Board.Move;
import engine.Board.Tile;
import engine.Color;
import engine.Pieces.*;
import engine.Player.BlackPlayer;
import engine.Player.MoveTransition;
import engine.Player.WhitePlayer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

public class WhiteIsInCheck {
    Board gameBoard;
    Knight initBlackKnight;
    BlackPlayer blackPlayer;
    WhitePlayer whitePlayer;
    Collection<Piece> blackPieces;
    Collection<Piece> whitePieces;
    Collection<Move> whiteNormalMoves;
    Collection<Move> blackNormalMoves;
    List<Tile> gamerBoard;
    @Test
    public void whiteInCheck_BlackPieceWillMove_WhiteKingWillGetInCheck() {
        gameBoard = createBoardForTest();
        gamerBoard = Board.crateBoard(new BoardBuilder());
        blackPieces = AllActivePieces(gamerBoard,Color.BLACK);
        whitePieces = AllActivePieces(gamerBoard,Color.WHITE);
        whiteNormalMoves = gameBoard.allLegalMoves(whitePieces);
        blackNormalMoves = gameBoard.allLegalMoves(blackPieces);
        blackPlayer = new BlackPlayer(gameBoard,blackNormalMoves,whiteNormalMoves);
        whitePlayer = new WhitePlayer(gameBoard,whiteNormalMoves,blackNormalMoves);
        King initWhiteKing = (King) gameBoard.getTile(60).getPiece();
        King initBlackKing = (King) gameBoard.getTile(4).getPiece();
        initBlackKnight = (Knight) gameBoard.getTile(39).getPiece();

        //white king
        Assertions.assertEquals(initWhiteKing.getPiecesStringType(),PiecesStringType.KING);
        //black king
        Assertions.assertEquals(initBlackKing.getPiecesStringType(),PiecesStringType.KING);

        //black knight
        Assertions.assertEquals(initBlackKnight.getPiecesStringType(), PiecesStringType.KNIGHT);

        ArrayList<String> knightAllowedMoves = new ArrayList<>();
        for (Move move : initBlackKnight.calculateAllLegalMoves(gameBoard)){
            knightAllowedMoves.add(move.toString());
        }
        ArrayList<String> expectedKnightAllowedMoves = new ArrayList<>(){{add(new Move.NormalMove(gameBoard,22,initBlackKnight).toString());
            add(new Move.NormalMove(gameBoard,29,initBlackKnight).toString());
            add(new Move.NormalMove(gameBoard,45,initBlackKnight).toString());
            add(new Move.NormalMove(gameBoard,54,initBlackKnight).toString());}};
        Assertions.assertEquals(expectedKnightAllowedMoves,knightAllowedMoves);

        Move move = Move.MoveCreation.createMove(gameBoard,39,45);
        MoveTransition transition = gameBoard.currentPlayer().makeMove(move);
        gameBoard = transition.getTransitionBoard();

        Piece emptyCurrentPosition = gameBoard.getTile(39).getPiece();
        Assertions.assertNull(emptyCurrentPosition);
        initBlackKnight = (Knight) gameBoard.getTile(45).getPiece();
        blackPieces = AllActivePieces(gamerBoard,Color.BLACK);
        whitePieces = AllActivePieces(gamerBoard,Color.WHITE);
        whiteNormalMoves = gameBoard.allLegalMoves(whitePieces);
        blackNormalMoves = gameBoard.allLegalMoves(blackPieces);
        blackPlayer = new BlackPlayer(gameBoard,blackNormalMoves,whiteNormalMoves);
        whitePlayer = new WhitePlayer(gameBoard,whiteNormalMoves,blackNormalMoves);

        int newKnightPosition = initBlackKnight.getPosition();
        int newExpectedKnightPosition = 45;
        Assertions.assertEquals(newExpectedKnightPosition, newKnightPosition);

        ArrayList<String> newKnightAllowedMoves = new ArrayList<>();
        for (Move newMove : initBlackKnight.calculateAllLegalMoves(gameBoard)){
            newKnightAllowedMoves.add(newMove.toString());
        }

        ArrayList<String> newExpectedKnightAllowedMoves = new ArrayList<>(Arrays.asList("e5","g5","d4","h4","d2","h2","fxe1","g1"));
        Assertions.assertEquals(newExpectedKnightAllowedMoves,newKnightAllowedMoves);

        boolean isWhiteInCheck = gameBoard.currentPlayer().isInCheck();
        boolean isBlackInCheck = gameBoard.currentPlayer().getOpponent().isInCheck();

        Assertions.assertTrue(isWhiteInCheck);
        Assertions.assertFalse(isBlackInCheck);
    }

    public static Board createBoardForTest(){
        BoardBuilder builder = new BoardBuilder();
        //setting black pieces
        builder.setPiece(new King(4,Color.BLACK));
        builder.setPiece(new Knight(39,Color.BLACK));

        //setting white pieces
        builder.setPiece(new King(60,Color.WHITE));


        //setting who have the first move
        builder.setColorNextMove(Color.BLACK);

        return builder.buildBoard();
    }

    private Collection<Piece> AllActivePieces(List<Tile> gamerBoard, Color color) {
        final List<Piece> activePieces = new ArrayList<>();
        for (final Tile tile : gamerBoard){
            if (tile.isTileOccupied()){
                final Piece piece = tile.getPiece();
                if (piece.getPieceColor() == color){
                    activePieces.add(piece);
                }
            }
        }
        return activePieces;
    }

}
