import engine.Board.Board;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class BoardTest {
    @Test
    public void initialBoard() {
        final Board board = Board.createStandardBoard();
        Assertions.assertEquals(board.currentPlayer().getLegalMoves().size(), 20);
        Assertions.assertEquals(board.currentPlayer().getOpponent().getLegalMoves().size(), 20);
        Assertions.assertFalse(board.currentPlayer().isInCheck());
        Assertions.assertFalse(board.currentPlayer().isInCheckMate());
        Assertions.assertEquals(board.currentPlayer(), board.getWhitePlayer());
    }
}