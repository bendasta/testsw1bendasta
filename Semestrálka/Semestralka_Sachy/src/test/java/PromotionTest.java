import engine.Board.Board;
import engine.Board.BoardBuilder;
import engine.Board.Move;
import engine.Color;
import engine.Pieces.*;
import engine.Player.MoveTransition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Arrays;

public class PromotionTest {
    Board gameBoard;
    Piece pawn;
    @Test
    public void Promotion_WhitePawnWillMove_WhitePawnWillPromoteToQueen() {

        gameBoard = createBoardForTest();

        ByteArrayInputStream in = new ByteArrayInputStream("q".getBytes());
        System.setIn(in);

        pawn = gameBoard.getTile(9).getPiece();

        ArrayList<String> pawnAllowedMoves = new ArrayList<>();
        for (Move move : pawn.calculateAllLegalMoves(gameBoard)){
            pawnAllowedMoves.add(move.toString());
        }
        ArrayList<String> expectedPawnAllowedMoves = new ArrayList<>();
        expectedPawnAllowedMoves.add("prom");

        Assertions.assertEquals(expectedPawnAllowedMoves,pawnAllowedMoves);

        Move move = Move.MoveCreation.createMove(gameBoard,9,1);
        MoveTransition transition = gameBoard.currentPlayer().makeMove(move);
        gameBoard = transition.getTransitionBoard();

        Piece emptyCurrentPosition = gameBoard.getTile(9).getPiece();
        Assertions.assertEquals(null, emptyCurrentPosition);
        pawn = gameBoard.getTile(1).getPiece();

        int newPawnPosition = pawn.getPosition();
        int newExpectedPawnPosition = 1;

        Assertions.assertEquals(newExpectedPawnPosition, newPawnPosition);


        ArrayList<String> newQueenAllowedMoves = new ArrayList<>();
        for (Move newMove : pawn.calculateAllLegalMoves(gameBoard)){
            newQueenAllowedMoves.add(newMove.toString());
        }
        ArrayList<String> expectedQueenAllowedMoves = new ArrayList<>(Arrays.asList("a8", "c8", "d8", "bxe8", "a7", "b7", "b6", "b5", "b4", "b3",
                "b2", "b1", "c7", "d6", "e5", "f4", "g3", "h2"));

        Assertions.assertEquals(expectedQueenAllowedMoves,newQueenAllowedMoves);
        Assertions.assertEquals(gameBoard.getTile(1).getPiece().getPiecesStringType(), PiecesStringType.QUEEN);
        Assertions.assertEquals(gameBoard.getTile(1).getPiece().getPieceColor(), Color.WHITE);

    }

    public static Board createBoardForTest(){
        BoardBuilder builder = new BoardBuilder();
        //setting black pieces
        builder.setPiece(new King(4, Color.BLACK));
        builder.setPiece(new Pawn(9,Color.WHITE));

        //setting white pieces
        builder.setPiece(new King(60,Color.WHITE));


        //setting who have the first move
        builder.setColorNextMove(Color.WHITE);

        return builder.buildBoard();
    }

}
