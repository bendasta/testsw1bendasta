package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;


/***
 * Class for timer
 */
public class TimerPanel extends JPanel{
    /***
     * Label for timer
     */
    JLabel ChessTime;
    /***
     * Timer itself
     */
    Timer t;
    /***
     * Ints for timer
     */
    int sec, min, hour;
    /**
     *String for formatting
     */
    String ddSec,ddMin,ddHour;
    /***
     * Decimal format for timer
     */
    DecimalFormat df = new DecimalFormat("00");
    //Clock clock = new Clock();
    private static final Dimension panelDimension = new Dimension(500,30);

    TimerPanel(){
        this.setLayout(new BorderLayout());
        this.setPreferredSize(panelDimension);
        this.ChessTime = new JLabel();
        this.hour = 0;
        this.min = 0;
        this.sec = 0;
        this.t = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sec++;
                ddSec = df.format(sec);
                ddMin = df.format(min);
                ddHour = df.format(hour);
                ChessTime.setText(ddHour + ":" + ddMin + ":" + ddSec);
                if (sec == 60){
                    sec = 0;
                    min ++;
                    ChessTime.setText(ddHour + ":" + ddMin + ":" + ddSec);
                }
                else if (min == 60){
                    min = 0;
                    hour++;
                    ChessTime.setText(ddHour + ":" + ddMin + ":" + ddSec);
                }
            }
        });
        t.start();
        this.ChessTime.setFont(new Font("TimesRoman",Font.BOLD,20));
        this.add(ChessTime,BorderLayout.CENTER);
        this.setVisible(true);
    }
}
