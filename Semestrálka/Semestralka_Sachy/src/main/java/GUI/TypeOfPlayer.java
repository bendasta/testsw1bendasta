package GUI;

/***
 * Enum for choosing which player is human or PC
 */
public enum TypeOfPlayer {
    /***
     * Human
     */
    Human,
    /***
     * PC
     */
    PC
}
