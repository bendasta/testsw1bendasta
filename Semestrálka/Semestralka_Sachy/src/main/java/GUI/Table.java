package GUI;

import engine.Board.Board;
import engine.Board.BoardUtils;
import engine.Board.Move;
import engine.Board.Move.*;
import engine.Board.Tile;
import engine.Pieces.Piece;
import engine.Player.AI.MiniMax;
import engine.Player.AI.MoveStrategy;
import engine.Player.MoveTransition;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static engine.Board.Board.createStandardBoard;
import static javax.swing.SwingUtilities.isLeftMouseButton;
import static javax.swing.SwingUtilities.isRightMouseButton;

/***
 * Table class
 */
public class Table extends Observable {
    private final JFrame gameFrame;
    private final BoardPanel boardPanel;
    private final MovesPanel movesPanel;
    private final TimerPanel timerPanel;
    private final MadeMoves madeMoves;
    private Tile sourceTile;
    private Tile destTile;
    private Piece movedPiece;
    private Board chessBoard;
    private final GameSetup gameSetup;
    private Move computerMove;

    private final static Dimension frameDimension = new Dimension(650,650);
    private final static Dimension tilePanelDimension = new Dimension(10,10);
    private final static Dimension boardPanelDimension = new Dimension(400,350);
    private Color lightTileColor = Color.white;
    private Color darkTileColor = Color.gray;
    private static String iconPath = "usedImages/";

    private static Table instance = new Table();

    /***
     * Table
     */
    private Table() {
        this.gameFrame = new JFrame("Chess");
        this.gameFrame.setLayout(new BorderLayout());

        final JMenuBar menuBar = new JMenuBar();
        menuBar.add(creationFileMenu());
        menuBar.add(GameSetup());

        this.gameFrame.setJMenuBar(menuBar);
        this.gameFrame.setSize(frameDimension);
        this.chessBoard = createStandardBoard();
        this.movesPanel = new MovesPanel(this.gameFrame);
        this.timerPanel = new TimerPanel();
        this.madeMoves = new MadeMoves();
        this.boardPanel = new BoardPanel();
        this.gameSetup = new GameSetup(this.gameFrame,true);
        this.addObserver(new TableGameAIWatcher());
        this.gameFrame.add(this.boardPanel,BorderLayout.CENTER);
        this.gameFrame.add(this.movesPanel,BorderLayout.EAST);
        this.gameFrame.add(this.timerPanel,BorderLayout.NORTH);
        this.gameFrame.setVisible(true);
    }

    /***
     * Getter for instance of this class
     * @return instance of this class
     */
    public static Table get(){
        return instance;
    }
    /***
     * Getter for new instance of this class
     * @return new instance of this class
     */
    public static Table newTable(){
        instance = new Table();
        return instance;
    }
    /***
     * Creates bar with chose to exit or load from file
     * @return set JMenu for panel
     */
    private JMenu creationFileMenu(){
        final JMenu Menu = new JMenu("File");
        final JMenuItem openFile = new JMenuItem("Save chess game to file file");
        openFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    FileWriter myWriter = new FileWriter("chessGame.txt");
                    myWriter.write(chessBoard.toString());
                    myWriter.close();
                    System.out.println("Successfully wrote to the file.");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                System.out.println("Open that file");
            }
        });
        Menu.add(openFile);

        final JMenuItem exitItem = new JMenuItem("Exit");
        exitItem.addActionListener((e) -> {System.exit(0);});
        Menu.add(exitItem);

        return Menu;
    }

    private JMenu GameSetup(){
        final JMenu gameSetup = new JMenu("Setup");

        final JMenuItem setupGameMenuItem = new JMenuItem("Setup Game");
        setupGameMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Table.get().getGameSetup().promptUser();
                Table.get().setupUpdate(Table.get().getGameSetup());
            }
        });
        gameSetup.add(setupGameMenuItem);

        return gameSetup;
    }

    private void setupUpdate(final GameSetup gameSetup) {
        setChanged();
        notifyObservers(gameSetup);
    }

    /***
     * Method for show this Frame
     */
    public void show() {
        Table.get().getMoveLog().clear();
        Table.get().getMadeMovesPanel().redo(chessBoard,Table.get().getMoveLog());
        Table.get().getBoardPanel().drawBoard(Table.get().getGameBoard());
    }

    private static class TableGameAIWatcher implements Observer {

        @Override
        public void update(Observable o, Object arg) {
            if (Table.get().getGameSetup().isAIPlayer(Table.get().getGameBoard().currentPlayer()) && !Table.get().getGameBoard().currentPlayer().isInCheckMate()){
                final AiThinkTank thinkTank = new AiThinkTank();
                thinkTank.execute();
            }
        }
    }

    private static class AiThinkTank extends SwingWorker<Move,String>{
        private AiThinkTank(){
        }

        @Override
        protected Move doInBackground() throws Exception {
            final MoveStrategy miniMax = new MiniMax(4);

            final Move bestMove = miniMax.execute(Table.get().getGameBoard());

            return bestMove;
        }

        public void done(){
            try {
                final Move bestMove = get();

                Table.get().updateComputerMove(bestMove);
                Table.get().updateGameBoard(Table.get().getGameBoard().currentPlayer().makeMove(bestMove).getTransitionBoard());
                Table.get().getMoveLog().AddMove(bestMove);
                Table.get().getMadeMovesPanel().redo(Table.get().getGameBoard(), Table.get().madeMoves);
                Table.get().getBoardPanel().drawBoard(Table.get().getGameBoard());
                Table.get().moveMadeUpdate(TypeOfPlayer.PC);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    private void moveMadeUpdate(final TypeOfPlayer typeOfPlayer) {
        setChanged();
        notifyObservers(typeOfPlayer);
    }

    private BoardPanel getBoardPanel() {
        return this.boardPanel;
    }

    private MovesPanel getMadeMovesPanel() {
        return this.movesPanel;
    }

    private MadeMoves getMoveLog() {
        return this.madeMoves;
    }

    /***
     * Method for update Computer move
     * @param move Board on which you want to make update
     */
    public void updateComputerMove(Move move) {
        this.computerMove = move;
    }

    /***
     * Method for update chess board
     * @param board Board on which you want to make update
     */
    public void updateGameBoard(final Board board) {
        this.chessBoard = board;
    }

    private Board getGameBoard() {
        return this.chessBoard;
    }

    /***
     * Getter for game setup
     * @return game setup
     */
    private GameSetup getGameSetup() {
        return this.gameSetup;
    }


    private class BoardPanel extends JPanel{
        final List<TilePanel> boardTiles;

        /***
         * Board panel
         */
        public BoardPanel() {
            super(new GridLayout(8,8));
            this.boardTiles = new ArrayList<>();
            for (int i = 0; i < BoardUtils.NumberOfTiles; i++) {
                final TilePanel tilePanel = new TilePanel(this,i);
                this.boardTiles.add(tilePanel);
                add(tilePanel);
            }
            setPreferredSize(boardPanelDimension);
            validate();
        }

        /***
         * Method to draw all tiles in pattern you want to tile panel
         * @param chessBoard which u want to draw
         */
        public void drawBoard(final Board chessBoard) {
            removeAll();
            for (final TilePanel tilePanel : boardTiles){
                tilePanel.drawTile(chessBoard);
                add(tilePanel);
            }
            validate();
            repaint();
        }
    }
    private class TilePanel extends JPanel{
        private final int tileID;

        private TilePanel(final BoardPanel boardPanel,final int tileID){
            super(new GridLayout());
            this.tileID = tileID;
            setPreferredSize(tilePanelDimension);
            assignTileColor();
            assignPieceIcon(chessBoard);

            addMouseListener(new MouseListener() {

                /***
                 * Method to moving pieces with your mouse
                 * @param e Mouse event
                 */
                @Override
                public void mouseClicked(final MouseEvent e) {
                    if (isRightMouseButton(e)){
                        sourceTile = null;
                        destTile = null;
                        movedPiece = null;
                    }
                    else if (isLeftMouseButton(e)){
                        if (sourceTile == null){
                            sourceTile = chessBoard.getTile(tileID);
                            movedPiece = sourceTile.getPiece();

                            if (movedPiece == null){
                                sourceTile = null;
                            }
                        }
                        else {
                            destTile = chessBoard.getTile(tileID);
                            final Move move = MoveCreation.createMove(chessBoard,sourceTile.getTileCoords(),destTile.getTileCoords());
                            final MoveTransition transition = chessBoard.currentPlayer().makeMove(move);
                            if (transition.getMoveStatus().isDone()){
                                chessBoard = transition.getTransitionBoard();
                                madeMoves.AddMove(move);
                            }
                            sourceTile = null;
                            destTile = null;
                            movedPiece = null;
                        }
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                movesPanel.redo(chessBoard,madeMoves);
                                if (gameSetup.isAIPlayer(chessBoard.currentPlayer())){
                                    Table.get().moveMadeUpdate(TypeOfPlayer.Human);
                                }

                                boardPanel.drawBoard(chessBoard);
                            }
                        });
                    }
                }
                @Override
                public void mousePressed(final MouseEvent e) {
                }

                @Override
                public void mouseReleased(final MouseEvent e) {
                }

                @Override
                public void mouseEntered(final MouseEvent e) {
                }

                @Override
                public void mouseExited(final MouseEvent e) {
                }
            });
            validate();
        }

        /***
         * Method to draw exact one chose tile
         * @param chessBoard where is that tile
         */
        public void drawTile(Board chessBoard) {
            assignTileColor();
            assignPieceIcon(chessBoard);
            highlightLegalMoves(chessBoard);
            validate();
            repaint();
        }

        /***
         * Method to highlight all piece legal move after you click on it
         * @param board where is that piece
         */
        private void highlightLegalMoves(final Board board){
            if (true){
                for (final Move move : pieceLegalMoves(board)){
                    if (move.getDestCoords() == this.tileID){
                        try {
                            add(new JLabel(new ImageIcon(ImageIO.read(new File("usedImages/green_dot.png")))));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        /***
         * Method for choosing piece all legal moves
         * @param board Current board
         * @return Collection of piece legal moves
         */
        private Collection<Move> pieceLegalMoves(Board board){
            if(movedPiece != null && movedPiece.getPieceColor() == board.currentPlayer().getColor()){
                return movedPiece.calculateAllLegalMoves(board);
            }
            return Collections.emptyList();
        }

        /***
         * Method to assign chosen Icon to a specific piece
         * @param board where is that piece
         */
        private void assignPieceIcon(final Board board){
            this.removeAll();
            if(board.getTile(this.tileID).isTileOccupied()){
                final BufferedImage tileImage;
                try {
                    tileImage = ImageIO.read(new File(iconPath +
                            board.getTile(this.tileID).getPiece().getPieceColor().toString().substring(0,1) + board.getTile(this.tileID).getPiece().toString() + ".gif"));
                    add(new JLabel(new ImageIcon(tileImage)));

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        /***
         * Method to assign tile colors - normal setup of chess board
         */
        private void assignTileColor() {
            if (BoardUtils.FIRST_ROW[this.tileID] || BoardUtils.THIRD_ROW[this.tileID] || BoardUtils.FIFTH_ROW[this.tileID] || BoardUtils.SEVENTH_ROW[this.tileID]){
                if (this.tileID % 2 == 0){
                    setBackground(lightTileColor);
                }
                else {
                    setBackground(darkTileColor);
                }
            }
            else if (BoardUtils.SECOND_ROW[this.tileID] || BoardUtils.FORTH_ROW[this.tileID] || BoardUtils.SIXTH_ROW[this.tileID] || BoardUtils.EIGHT_ROW[this.tileID]){
                if (this.tileID % 2 != 0){
                    setBackground(lightTileColor);
                }
                else {
                    setBackground(darkTileColor);
                }
            }
        }
    }

    /***
     * Class for table of made moves
     */
    public static class MadeMoves{
        private final List<Move> moves;

        MadeMoves(){
            this.moves = new ArrayList<>();
        }

        /***
         * Method for getting moves
         * @return List of moves
         */
        public List<Move> getMoves() {
            return this.moves;
        }

        /***
         * Method for adding move
         * @param move Move to add
         */
        public void AddMove(final Move move){
            this.moves.add(move);
        }

        /***
         * Clearing list
         */
        public void clear(){
            this.moves.clear();
        }

        /***
         * Method for removing item by index
         * @param index of chosen item
         * @return List with removed item
         */
        public Move removeMove(int index){
            return this.moves.remove(index);
        }

        /***
         * Method for removing move from list
         * @param move Move to remove
         * @return boolean
         */
        public boolean removeMove(final Move move){
            return this.moves.remove(move);
        }

        /***
         * Getter for size
         * @return size of List
         */
        public int size(){
           return this.moves.size();
        }
    }
}
