package GUI;

import engine.Board.Board;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import GUI.Table.*;
import engine.Board.Move;

/***
 * Class for panel where are moves
 */
public class MovesPanel extends JPanel {
    /***
     * Model for data
     */
    private final DataModel model;
    /***
     * Scroll pane for moves
     */
    private final JScrollPane scrollPane;
    private static final Dimension panelDimension = new Dimension(100,500);
    final static Logger logger = Logger.getLogger(Move.class.getName());
    /***
     * Timer
     */
    TimerPanel timer = new TimerPanel();
    /***
     * JFrame to close
     */
    private final JFrame chess;

    MovesPanel(JFrame chess){
        this.setLayout(new BorderLayout());
        this.model = new DataModel();
        final JTable table = new JTable(model);
        table.setRowHeight(15);
        this.chess = chess;
        this.scrollPane = new JScrollPane(table);
        scrollPane.setColumnHeaderView(table.getTableHeader());
        scrollPane.setPreferredSize(panelDimension);
        this.add(scrollPane,BorderLayout.CENTER);
        this.setVisible(true);
    }

    /***
     * Method to update moves panel
     * @param board where are these moves
     * @param madeMoves which were make by player in his move
     */
    void redo(final Board board, final MadeMoves madeMoves){
        int currRow = 0;
        this.model.clear();
        for (final Move move: madeMoves.getMoves()){
            final String moveText = move.toString();
            if (move.getMovedPiece().getPieceColor().isWhite()){
                this.model.setValueAt(moveText,currRow,0);
            }
            else if (move.getMovedPiece().getPieceColor().isBlack()){
                this.model.setValueAt(moveText,currRow,1);
                currRow++;
            }
        }

        if (madeMoves.getMoves().size() > 0){
            final Move lastMove = madeMoves.getMoves().get(madeMoves.size()-1);
            final String moveText = lastMove.toString();

            if (lastMove.getMovedPiece().getPieceColor().isWhite()){
                this.model.setValueAt(moveText + calculateCheckAndCheckMate(board),currRow,0);
            }
            else if (lastMove.getMovedPiece().getPieceColor().isBlack()){
                this.model.setValueAt(moveText + calculateCheckAndCheckMate(board), currRow -1,1);
            }
        }

        final JScrollBar vertical = scrollPane.getVerticalScrollBar();
        vertical.setValue(vertical.getMaximum());
    }

    /***
     * Method to find if there is a check of check mate
     * @param board  incoming built board with pieces
     * @return symbol for check or check mate for MovesPanel
     */
    private String calculateCheckAndCheckMate(final Board board) {
        if (board.currentPlayer().isInCheckMate()){
            Dimension frameDimension = new Dimension(400,200);
            JFrame winner = new JFrame("Winner");
            JPanel panel = new JPanel();
            JButton close = new JButton("Close");
            JButton newGame = new JButton("New Game");
            close.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    System.exit(0);
                }
            });
            newGame.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Table.newTable().show();
                    winner.dispose();
                }
            });
            winner.setSize(frameDimension);
            winner.setLayout(new BorderLayout());
            timer.t.stop();
            JLabel winnerLabel = new JLabel("Winner is " + board.currentPlayer().getOpponent().getColor().toString() + " In time: " + this.timer.ChessTime.getText());
            winnerLabel.setFont(new Font("Sans-serif",Font.BOLD,20));
            panel.add(winnerLabel,BorderLayout.CENTER);
            winner.add(panel,BorderLayout.NORTH);
            winner.add(newGame,BorderLayout.CENTER);
            winner.add(close,BorderLayout.SOUTH);
            winner.setVisible(true);
            chess.dispose();
            logger.info(board.currentPlayer().getColor() + " King is in Check Mate - you lost");
            return "#";
        }
        else if (board.currentPlayer().isInCheck()){
            logger.info(board.currentPlayer().getColor() + " King is in check");
            return "+";
        }
        return "";
    }

    private static class DataModel extends DefaultTableModel {
        private final List<Row> values;
        private static final String[] headers = {"White","Black"};

        DataModel(){
            this.values = new ArrayList<>();
        }

        /***
         * Method for clearing table
         */
        public void clear(){
            this.values.clear();
            setRowCount(0);
        }

        /***
         * Get row count
         * @return Row count
         */
        @Override
        public int getRowCount() {
            if (this.values == null){
                return 0;
            }
            return this.values.size();
        }

        /***
         * get column count
         * @return column count
         */
        @Override
        public int getColumnCount() {
            return headers.length;
        }

        /***
         * Get value at
         * @param row number
         * @param column number
         * @return value at chosen row and column
         */
        @Override
        public Object getValueAt(final int row,final int column) {
            final Row currentRow = this.values.get(row);
            if (column == 0){
                return currentRow.getWhiteMove();
            }
            else if (column == 1){
                return currentRow.getBlackMove();
            }
            return null;
        }

        /***
         * Set value at
         * @param aValue value
         * @param row number
         * @param column number
         */
        @Override
        public void setValueAt(Object aValue, int row, int column) {
            final Row currentRow;
            if (this.values.size() <= row){
                currentRow = new Row();
                this.values.add(currentRow);
            }
            else{
                currentRow = this.values.get(row);
            }
            if (column == 0){
                currentRow.setWhiteMove((String) aValue);
                fireTableRowsInserted(row,row);
            }
            else if (column == 1){
                currentRow.setBlackMove((String) aValue);
                fireTableCellUpdated(row,column);
            }
        }

        /***
         * Get column class
         * @param columnIndex column index
         * @return column class
         */
        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return Move.class;
        }

        /***
         * Get column name
         * @param column number
         * @return column header
         */
        @Override
        public String getColumnName(int column) {
            return headers[column];
        }
    }

    private static class Row{
        private String whiteMove;
        private String blackMove;
        Row(){

        }

        /***
         * Get white move
         * @return white move
         */
        public String getWhiteMove() {
            return whiteMove;
        }

        /***
         * Set white move
         * @param whiteMove white move
         */
        public void setWhiteMove(String whiteMove) {
            this.whiteMove = whiteMove;
        }

        /***
         * Get black move
         * @return black move
         */
        public String getBlackMove() {
            return blackMove;
        }

        /***
         * Set black move
         * @param blackMove black move
         */
        public void setBlackMove(String blackMove) {
            this.blackMove = blackMove;
        }
    }

}
