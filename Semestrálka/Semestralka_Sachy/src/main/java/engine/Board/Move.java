package engine.Board;

import engine.Pieces.Pawn;
import engine.Pieces.Piece;
import engine.Pieces.Rook;

/***
 * Move class
 */
public abstract class Move {

    protected Board board;
    protected int destinationCoords;
    protected Piece movedPiece;
    private boolean isFirstMove;
    /***
     * Null declaration move on board
     */
    public static final Move nullMove = new NullMove();

    private Move(final Board board,final int destinationCoords,final Piece movedPiece) {
        this.board = board;
        this.destinationCoords = destinationCoords;
        this.movedPiece = movedPiece;
        this.isFirstMove = movedPiece.isFirstMove();
    }
    private Move(final Board board, final int destinationCoords)
    {
        this.board = board;
        this.destinationCoords = destinationCoords;
        this.movedPiece = null;
        this.isFirstMove = false;
    }


    /***
     * Get dest coords
     * @return destination coordinate
     */
    public int getDestCoords() {
        return this.destinationCoords;
    }

    /***
     * Get moved piece
     * @return moved piece
     */
    public Piece getMovedPiece(){
        return this.movedPiece;
    }

    /***
     * Is attack
     * @return default value (false)
     */
    public boolean isAttack(){
        return false;
    }

    /***
     * Is Castling Move
     * @return default value (false)
     */
    public boolean isCastlingMove(){
        return false;
    }

    /***
     * Get attacked piece
     * @return default value (Null)
     */
    public Piece getAttackPiece(){
        return null;
    }
    private int getCurrentCoords() { return this.getMovedPiece().getPosition();}

    /***
     * Get board
     * @return board
     */
    public Board getBoard(){return this.board;}

    /***
     * Equals method
     * @param other other item
     * @return boolean
     */
    @Override
    public boolean equals(Object other) {
        if (this == other){
            return true;
        }
        if (!(other instanceof Piece)){
            return false;
        }

        final Move otherMove = (Move) other;
        return getDestCoords() == movedPiece.getPosition() && getMovedPiece().equals(otherMove.getMovedPiece());
    }

    /**
     * Hash code method
     * @return hash code
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int res = 1;

        res = prime * res + this.destinationCoords;
        res = prime * res + this.movedPiece.hashCode();

        return  res;
    }
    /***
     * Executes type of move you want to make
     * @return new Board with executed move
     */
    public Board execute(){
        final BoardBuilder builder = new BoardBuilder();

        for (final Piece piece : this.board.currentPlayer().getAllActivePieces()){
            if (!this.movedPiece.equals(piece)){
                builder.setPiece(piece);
            }
        }
        for (final Piece piece : this.board.currentPlayer().getOpponent().getAllActivePieces()){
            builder.setPiece(piece);
        }

        //moving the chosen piece
        builder.setPiece(this.movedPiece.movePiece(this));
        builder.setColorNextMove(this.board.currentPlayer().getOpponent().getColor());
        return builder.buildBoard();
    }

    /***
     * Class for normal move
     */
    public static final class NormalMove extends Move {
        /***
         * Constructor for Normal move
         * @param board board
         * @param destinationCoords destcoords
         * @param movedPiece moved piece
         */
        public NormalMove(final Board board,final int destinationCoords,final Piece movedPiece) {
            super(board, destinationCoords, movedPiece);
        }
        @Override
        public String toString() {
            return BoardUtils.getPositionAtCoords(this.destinationCoords);
        }

    }

    /***
     * Class for attack move
     */
    public static class AttackMove extends Move {
        final Piece attackedPiece;

        /***
         * Constructor for Attack move
         * @param board board
         * @param destinationCoords destcoords
         * @param movedPiece moved piece
         * @param attackedPiece attacked piece
         */
        public AttackMove(final Board board,final int destinationCoords,final Piece movedPiece,final Piece attackedPiece) {
            super(board, destinationCoords, movedPiece);
            this.attackedPiece = attackedPiece;
        }

        /***
         * Is attack
         * @return bolean
         */
        @Override
        public boolean isAttack() {
            return true;
        }

        /***
         * Get attack piece
         * @return attack pice
         */
        @Override
        public Piece getAttackPiece() {
            return this.attackedPiece;
        }

        /***
         * Hashcode representation
         * @return hashcode representation
         */
        @Override
        public int hashCode() {
            return this.attackedPiece.hashCode() + super.hashCode();
        }
        /***
         * Equals method
         * @param other other item
         * @return boolean
         */
        @Override
        public boolean equals(Object other) {
            if (this == other){
                return true;
            }
            if (!(other instanceof Piece)){
                return false;
            }

            final AttackMove otherAttackMove = (AttackMove) other;
            return super.equals(otherAttackMove) && getAttackPiece().equals(otherAttackMove.getAttackPiece());
        }
        /***
         * To string method
         * @return string representation
         */
        @Override
        public String toString() {
            return BoardUtils.getPositionAtCoords(this.movedPiece.getPosition()).substring(0,1) + "x" + BoardUtils.getPositionAtCoords(this.destinationCoords);
        }
    }

    /***
     * Class for PawnMove
     */
    public static final class PawnMove extends Move {
        /***
         * Constructor for Pawn move
         * @param board board
         * @param destinationCoords destcoords
         * @param movedPiece moved piece
         */
        public PawnMove(final Board board,final int destinationCoords,final Piece movedPiece) {
            super(board, destinationCoords, movedPiece);
        }

        /***
         * Equals method
         * @param other other item
         * @return boolean
         */
        @Override
        public boolean equals(Object other) {
            return this == other || other instanceof PawnMove && super.equals(other);
        }

        /***
         * To string method
         * @return string representation
         */
        @Override
        public String toString() {
            return BoardUtils.getPositionAtCoords(this.destinationCoords);
        }
    }

    /***
     * Class for Pawn attack move
     */
    public static class PawnAttackMove extends AttackMove {
        /***
         * Constructor for Pawn Attack move
         * @param board board
         * @param destinationCoords destcoords
         * @param movedPiece moved piece
         * @param attackedPiece attacked piece
         */
        public PawnAttackMove(final Board board, final int destinationCoords, final Piece movedPiece,final Piece attackedPiece) {
            super(board, destinationCoords, movedPiece,attackedPiece);
        }

        /***
         * Hashcode representation
         * @return hashcode representation
         */
        @Override
        public int hashCode() {
            return super.hashCode();
        }
        /***
         * Equals method
         * @param other other item
         * @return boolean
         */
        @Override
        public boolean equals(Object other) {
            return this == other || other instanceof PawnAttackMove && super.equals(other);
        }
        /***
         * To string method
         * @return string representation
         */
        @Override
        public String toString() {
            return BoardUtils.getPositionAtCoords(this.movedPiece.getPosition()).substring(0,1) + "x" + BoardUtils.getPositionAtCoords(this.destinationCoords);
        }
    }

    /***
     * Class for pawn jump
     */
    public static final class PawnJump extends Move {
        /***
         * Constructor for Pawn jump
         * @param board board
         * @param destinationCoords destination coords
         * @param movedPiece moved piece
         */
        public PawnJump(Board board, int destinationCoords, Piece movedPiece) {
            super(board, destinationCoords, movedPiece);
        }

        /***
         * Executes Pawn jump you want to make
         * @return new Board with executed Pawn jump
         */
        @Override
        public Board execute() {
            final BoardBuilder builder = new BoardBuilder();

            for (final Piece piece : this.board.currentPlayer().getAllActivePieces()) {
                if (!this.movedPiece.equals(piece)) {
                    builder.setPiece(piece);
                }
            }
            for (final Piece piece : this.board.currentPlayer().getOpponent().getAllActivePieces()) {
                builder.setPiece(piece);
            }

            final Pawn movedPawn = (Pawn) this.movedPiece.movePiece(this);

            builder.setPiece(movedPawn);
            builder.setEnPassantPawn(movedPawn);
            builder.setColorNextMove(this.board.currentPlayer().getOpponent().getColor());

            return builder.buildBoard();
        }
        /***
         * To string method
         * @return string representation
         */
        @Override
        public String toString() {
            return BoardUtils.getPositionAtCoords(this.destinationCoords);
        }
    }

    /***
     * Class for pawn promotion
     */
    public static class PawnPromotion extends Move{
        final Move decMove;
        final Pawn promotedPawn;

        /***
         * Constructor for Pawn promotion move class
         * @param decMove Move to make
         */
        public PawnPromotion(final Move decMove) {
            super(decMove.getBoard(), decMove.getDestCoords(), decMove.getMovedPiece());
            this.decMove = decMove;
            this.promotedPawn = (Pawn) decMove.getMovedPiece();
        }

        /***
         * Executes Pawn promotion you want to make
         * @return new Board with executed Pawn promotion
         */
        @Override
        public Board execute() {
            final Board pawnMovedBoard = this.decMove.execute();
            final BoardBuilder builder = new BoardBuilder();
            for (final Piece piece : pawnMovedBoard.currentPlayer().getAllActivePieces()){
                if (!this.promotedPawn.equals(piece)){
                    builder.setPiece(piece);
                }
            }
            for (final Piece piece : pawnMovedBoard.currentPlayer().getOpponent().getAllActivePieces()){
                builder.setPiece(piece);
            }
            builder.setPiece(this.promotedPawn.getPromotionPiece().movePiece(this));
            builder.setColorNextMove(pawnMovedBoard.currentPlayer().getColor());
            return builder.buildBoard();
        }

        /***
         * Is attack
         * @return boolean is attack
         */
        @Override
        public boolean isAttack() {
            return this.decMove.isAttack();
        }

        /***
         * Get attacked piece
         * @return attacked piece
         */
        @Override
        public Piece getAttackPiece() {
            return this.decMove.getAttackPiece();
        }
        /***
         * To string method
         * @return string representation
         */
        @Override
        public String toString() {
            return "prom";
        }
        /***
         * Equals method
         * @param other other item
         * @return boolean
         */
        @Override
        public boolean equals(Object other) {
            return this == other || other instanceof PawnPromotion && super.equals(other);
        }
        /***
         * Hashcode representation
         * @return hashcode representation
         */
        @Override
        public int hashCode() {
            return decMove.hashCode() + (31 * promotedPawn.hashCode());
        }
    }

    /***
     * Class for en-passant move
     */
    public static final class PawnEnPassantMove extends PawnAttackMove {
        /***
         * Constructor for Pawn En Passant move class
         * @param board board
         * @param destinationCoords destination coords
         * @param movedPiece moved piece
         * @param attackedPiece chose attacked piece
         */
        public PawnEnPassantMove(final Board board, final int destinationCoords, final Piece movedPiece, final Piece attackedPiece) {
            super(board, destinationCoords, movedPiece,attackedPiece);
        }
        /***
         * Equals method
         * @param other other item
         * @return boolean
         */
        @Override
        public boolean equals(Object other) {
            return this == other || other instanceof PawnEnPassantMove && super.equals(other);
        }

        /***
         * Executes EnPassant move you want to make
         * @return new Board with executed EnPassant move
         */
        @Override
        public Board execute() {
            final BoardBuilder builder = new BoardBuilder();
            for (final Piece piece : this.board.currentPlayer().getAllActivePieces()){
                if (!this.movedPiece.equals(piece)){
                    builder.setPiece(piece);
                }
            }
            for (final Piece piece : this.board.currentPlayer().getOpponent().getAllActivePieces()){
                if (!piece.equals(this.getAttackPiece())){
                    builder.setPiece(piece);
                }
            }

            builder.setPiece(this.movedPiece.movePiece(this));
            builder.setColorNextMove(this.board.currentPlayer().getOpponent().getColor());
            return builder.buildBoard();
        }
        /***
         * To string method
         * @return string representation
         */
        @Override
        public String toString() {
            return BoardUtils.getPositionAtCoords(this.movedPiece.getPosition()).substring(0,1) + "x" + BoardUtils.getPositionAtCoords(this.destinationCoords);
        }
    }
    static abstract class CastleMove extends Move {

        protected final Rook castleRook;
        protected final int castleRookStart;
        protected final int castleRookDest;

        private CastleMove(final Board board,final int destinationCoords,final Piece movedPiece, final Rook castleRook,final int castleRookStart, final int castleRookDest) {
            super(board, destinationCoords, movedPiece);
            this.castleRook = castleRook;
            this.castleRookStart = castleRookStart;
            this.castleRookDest = castleRookDest;
        }

        /***
         * Get castle rook
         * @return castle rook
         */
        public Rook getCastleRook() {
            return this.castleRook;
        }

        /***
         * Is castling move
         * @return default value (true)
         */
        @Override
        public boolean isCastlingMove() {
            return true;
        }

        /***
         * Executes Castle move you want to make
         * @return new Board with executed Castle move
         */
        @Override
        public Board execute() {
            final BoardBuilder builder = new BoardBuilder();
            for (final Piece piece : this.board.currentPlayer().getAllActivePieces()){
                if (!this.movedPiece.equals(piece) && !this.castleRook.equals(piece)){
                    builder.setPiece(piece);
                }
            }
            for (final Piece piece : this.board.currentPlayer().getOpponent().getAllActivePieces()) {
                builder.setPiece(piece);
            }

            builder.setPiece(this.movedPiece.movePiece(this));
            builder.setPiece(new Rook(this.castleRookDest,this.castleRook.getPieceColor(),false));
            builder.setColorNextMove(this.board.currentPlayer().getOpponent().getColor());

            return builder.buildBoard();
        }
        /***
         * Equals method
         * @param other other item
         * @return boolean
         */
        @Override
        public boolean equals(Object other) {
            if (this == other){
                return true;
            }
            if (!(other instanceof CastleMove)){
                return false;
            }

            final CastleMove otherCastleMove = (CastleMove) other;
            return super.equals(otherCastleMove) && this.castleRook.equals(otherCastleMove.getCastleRook());
        }
        /***
         * Hashcode representation
         * @return hashcode representation
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int res = super.hashCode();
            res = prime * res + this.castleRook.hashCode();
            res = prime * res + this.castleRookDest;
            return res;
        }
    }

    /***
     * Class for KingSideCastleMove
     */
    public static final class KingSideCastleMove extends CastleMove {
        /***
         * Constructor for KingSideCastleMove
         * @param board board
         * @param destinationCoords dest coords
         * @param movedPiece moved piece
         * @param castleRook castle rook
         * @param castleRookStart castle rook start
         * @param castleRookDest castle rook destinantion
         */
        public KingSideCastleMove(final Board board, final int destinationCoords, final Piece movedPiece, final Rook castleRook, final int castleRookStart, final int castleRookDest) {
            super(board, destinationCoords, movedPiece,castleRook,castleRookStart,castleRookDest);
        }
        /***
         * Equals method
         * @param other other item
         * @return boolean
         */
        @Override
        public boolean equals(Object other) {
            return this == other || other instanceof KingSideCastleMove && super.equals(other);
        }
        /***
         * To string method
         * @return string representation
         */
        @Override
        public String toString() {
            return "0-0";
        }
    }

    /***
     * Class for queen side castle move
     */
    public static final class QueenSideCastleMove extends CastleMove {
        /***
         * Constructor for QueenSideCastleMove
         * @param board board
         * @param destinationCoords dest coords
         * @param movedPiece moved piece
         * @param castleRook castle rook
         * @param castleRookStart castle rook start
         * @param castleRookDest castle rook destinantion
         */
        public QueenSideCastleMove(final Board board, final int destinationCoords, final Piece movedPiece, final Rook castleRook, final int castleRookStart, final int castleRookDest) {
            super(board, destinationCoords, movedPiece,castleRook,castleRookStart,castleRookDest);
        }
        @Override
        public boolean equals(Object other) {
            return this == other || other instanceof QueenSideCastleMove && super.equals(other);
        }
        /***
         * To string method
         * @return string representation
         */
        @Override
        public String toString() {
            return "0-0-0";
        }
    }

    /***
     * Class for null move
     */
    public static final class NullMove extends Move{
        /***
         * Constructor for null move class
         */
        public NullMove(){
            super(null,-1);
        }
        /***
         * Executes Castle move you want to make
         * @return new Board with executed Castle move
         */
        @Override
        public Board execute() {
            throw  new RuntimeException("You cant execute Null move!!");
        }
    }

    /***
     * Class for move creation
     */
    public static class MoveCreation{

        private MoveCreation() {
            throw new RuntimeException("This cant be initialized!!");
        }

        /***
         * Move creation - if there is same legal move on board it creates legal move otherwise it creates null move
         * @param board on which you want to make the move
         * @param currentCoords coords from where you want to go
         * @param destCoords coords where you want to go
         * @return created type of Move
         */
        public static Move createMove(final Board board,final int currentCoords,final int destCoords){
            for (final Move move : board.getAllLegalMoves()){
                if (move.getCurrentCoords() == currentCoords && move.getDestCoords() == destCoords ){
                    return move;
                }
            }
            return nullMove;
        }
    }
}