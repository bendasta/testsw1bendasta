package engine.Board;

import java.util.HashMap;
import java.util.Map;

/***
 * Board utils class
 */
public class BoardUtils {
    /***
     * If it is first column say it is true - initialization
     */
    public static final boolean[] FIRST_COLUMN = initColumn(0);
    /***
     * If it is second column say it is true - initialization
     */
    public static final boolean[] SECOND_COLUMN = initColumn(1);
    /***
     * If it is seventh column say it is true - initialization
     */
    public static final boolean[] SEVENTH_COLUMN = initColumn(6);
    /***
     * If it is eight column say it is true - initialization
     */
    public static final boolean[] EIGHT_COLUMN = initColumn(7);


    /***
     * If it is first row say it is true - initialization
     */
    public static final boolean[] FIRST_ROW = initRow(0);
    /***
     * If it is second row say it is true - initialization
     */
    public static final boolean[] SECOND_ROW = initRow(8);
    /***
     * If it is third row say it is true - initialization
     */
    public static final boolean[] THIRD_ROW = initRow(16);
    /***
     * If it is forth row say it is true - initialization
     */
    public static final boolean[] FORTH_ROW = initRow(24);
    /***
     * If it is fifth row say it is true - initialization
     */
    public static final boolean[] FIFTH_ROW = initRow(32);
    /***
     * If it is sixth row say it is true - initialization
     */
    public static final boolean[] SIXTH_ROW = initRow(40);
    /***
     * If it is seventh row say it is true - initialization
     */
    public static final boolean[] SEVENTH_ROW = initRow(48);
    /***
     * If it is eight row say it is true - initialization
     */
    public static final boolean[] EIGHT_ROW = initRow(56);

    /***
     * String notation of possible moves
     */
    public static final String[] notation = initNotation();

    /***
     * Number of tiles on chess board
     */
    public static final int NumberOfTiles = 64;
    /***
     * Number of tiles per row
     */
    public static final int NumberOfTilesPerRow = 8;

    //Just utilities, this can't be initialized
    private BoardUtils(){
        throw new RuntimeException("You cant!!!");
    }

    /***
     *
     * @param coords input coordination
     * @return boolean true if input coords are valid or false if they are invalid
     */
    public static boolean isTileCoorsValid(int coords){
        if (coords >= 0 && coords < NumberOfTiles){
            return true;
        }
        return false;
    }

    /***
     *
     * @param columnNumber number of column you want to initialize
     * @return array of boolean where value true is for input column
     */
    private static boolean[] initColumn(int columnNumber) {
        final boolean[] column = new boolean[NumberOfTiles];
        do {
            column[columnNumber] = true;
            columnNumber += NumberOfTilesPerRow;
        }while (columnNumber < NumberOfTiles);

        return column;
    }

    /***
     * Just initialize string notation of tiles
     * @return string array of string tile notation
     */
    private static String[] initNotation() {
        return new String[] {
                "a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8",
                "a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7",
                "a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6",
                "a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5",
                "a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4",
                "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3",
                "a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2",
                "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"
        };
    }

    /***
     * @param rowNumber number of row you want to initialize
     * @return array of boolean where value true is for input row
     */
    private static boolean[] initRow(int rowNumber){
        final boolean[] row = new boolean[NumberOfTiles];
        do {
            row[rowNumber] = true;
            rowNumber ++;
        }
        while (rowNumber % NumberOfTilesPerRow !=0);
        return row;
    }

    /***
     * @param destinationCoords coords where you want to know string notation
     * @return String which represents String notation of tile on destCoords
     */
    public static String getPositionAtCoords(final int destinationCoords) {
        return notation[destinationCoords];
    }
}
