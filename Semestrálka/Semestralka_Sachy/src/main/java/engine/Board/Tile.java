package engine.Board;

import engine.Pieces.Piece;

import java.util.HashMap;
import java.util.Map;

/***
 * Abstract class for tile and other tiles
 */
public abstract class Tile {
    protected final int tileCoords;
    private static final Map<Integer,EmptyTile> EmptyTiles = createAllPossibleEmptyTiles();

    /***
     * Method for building a map of 64 empty tiles
     * @return Map where all tiles are empty
     */
    private static Map<Integer, EmptyTile> createAllPossibleEmptyTiles() {
        final Map<Integer,EmptyTile> EmptyTileMap = new HashMap<>();

        for (int i = 0; i < BoardUtils.NumberOfTiles ;i++){
            EmptyTileMap.put(i,new EmptyTile(i));
        }
        return EmptyTileMap;
    }

    private Tile(final int tileCoords){
        this.tileCoords = tileCoords;
    }

    /***
     * Method to override is tile occupied
     * @return boolean for tile occupation
     */
    public abstract boolean isTileOccupied();

    /***
     * Method to override . Get piece
     * @return Piece
     */
    public abstract Piece getPiece();

    /***
     * Get tile coords
     * @return tile coordination
     */
    public int getTileCoords(){
        return this.tileCoords;
    }

    /***
     * Tile creation
     * @param tileCoords of creating tile
     * @param piece piece on tile
     * @return created Tile
     */
    public static Tile createTile(final int tileCoords, final Piece piece){
        return piece != null ? new OccupiedTile(tileCoords,piece) : EmptyTiles.get(tileCoords);
    }

    /***
     * Class for empty tile
     */
    public static final class EmptyTile extends Tile{
        EmptyTile(final int tileCoords){
            super(tileCoords);
        }

        /***
         * Method is tile occupied
         * @return default value
         */
        @Override
        public boolean isTileOccupied() {
            return false;
        }

        /***
         * Get piece
         * @return default value
         */
        @Override
        public Piece getPiece() {
            return null;
        }

        /***
         * To string method
         * @return string representation for console
         */
        @Override
        public String toString() {
            return "-";
        }
    }

    /***
     * Class for occupied tile
     */
    public static final class OccupiedTile extends Tile{
        private final Piece tilePiece;

        OccupiedTile(int tileCoords, Piece tilePiece) {
            super(tileCoords);
            this.tilePiece = tilePiece;
        }

        /***
         * Is tile occupied
         * @return default value
         */
        @Override
        public boolean isTileOccupied() {
            return true;
        }

        /***
         * Get piece
         * @return peice on tile
         */
        @Override
        public Piece getPiece() {
            return this.tilePiece;
        }

        /***
         * To string method
         * @return string representation for console
         */
        @Override
        public String toString() {
            if (getPiece().getPieceColor().isBlack()){
                return getPiece().toString().toLowerCase();
            }
            else{
                return getPiece().toString();
            }
        }
    }
}
