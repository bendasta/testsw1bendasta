package engine.Board;

import engine.Color;
import engine.Pieces.*;
import engine.Player.BlackPlayer;
import engine.Player.Player;
import engine.Player.WhitePlayer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/***
 * Board class
 */
public class Board {

    private final List<Tile> gamerBoard;
    private final Collection<Piece> blackPieces;
    private final Collection<Piece> whitePieces;
    private final BlackPlayer blackPlayer;
    private final WhitePlayer whitePlayer;
    protected Collection<Move> AllLegalsMoves;
    private final Player currentPlayer;
    private final Pawn enPassantPawn;

    /***
     * Board Constructor
     * @param builder for building this board
     */
    public Board(BoardBuilder builder) {
        this.gamerBoard = crateBoard(builder);
        this.blackPieces = AllActivePieces(this.gamerBoard,Color.BLACK);
        this.whitePieces = AllActivePieces(this.gamerBoard,Color.WHITE);
        this.enPassantPawn = builder.enPassantPawn;
        final Collection<Move> whiteNormalMoves = Collections.unmodifiableCollection(allLegalMoves(this.whitePieces));
        final Collection<Move> blackNormalMoves = Collections.unmodifiableCollection(allLegalMoves(this.blackPieces));
        this.blackPlayer = new BlackPlayer(this,blackNormalMoves,whiteNormalMoves);
        this.whitePlayer = new WhitePlayer(this,whiteNormalMoves,blackNormalMoves);
        this.currentPlayer = builder.colorNextMove.chosePlayer(this.whitePlayer,this.blackPlayer);
        this.AllLegalsMoves = getAllLegalBoardMoves();
    }
    //Finding all active pieces on board

    /***
     * Method for find all active pieces of chosen color
     * @param gamerBoard board where you want to find pieces
     * @param color color of pieces you want to find
     * @return Collection<Pieces> of chosen color </>
     */
    private Collection<Piece> AllActivePieces(List<Tile> gamerBoard, Color color) {
        final List<Piece> activePieces = new ArrayList<>();
        for (final Tile tile : gamerBoard){
            if (tile.isTileOccupied()){
                final Piece piece = tile.getPiece();
                if (piece.getPieceColor() == color){
                    activePieces.add(piece);
                }
            }
        }
        return activePieces;
    }

    /***
     * Method to find all legal moves of input Collection of pieces
     * @param pieces which are active and same color
     * @return Collection of all legal moves for input Collection of pieces
     */
    public Collection<Move> allLegalMoves(final Collection<Piece> pieces){
        final List<Move> legalMoves = new ArrayList<>();
        for (final Piece piece : pieces){
            legalMoves.addAll(piece.calculateAllLegalMoves(this));
        }
        return legalMoves;
    }

    /***
     * Method to create Board of 64 tiles
     * @param builder Board builder which have some board creation methods
     * @return List of tiles and pieces if there are some
     */
    public static List<Tile> crateBoard(final BoardBuilder builder){
        final Tile[] tiles = new Tile[BoardUtils.NumberOfTiles];
        for (int i = 0; i < BoardUtils.NumberOfTiles; i++) {
            tiles[i] = Tile.createTile(i,builder.boardMapping.get(i));
        }
        return List.of(tiles);
    }

    /***
     * Get black player
     * @return board black player
     */
    public Player getBlackPlayer(){
        return this.blackPlayer;
    }

    /***
     * Get white player
     * @return board white player
     */
    public Player getWhitePlayer(){
        return this.whitePlayer;
    }

    /***
     * Get Current player
     * @return board current player
     */
    public Player currentPlayer() {
        return this.currentPlayer;
    }

    /***
     * Get En passant pawn
     * @return return en passant pawn
     */
    public Pawn getEnPassantPawn(){
        return this.enPassantPawn;
    }

    /***
     * Method to merge Collections of both colors legal moves
     * @return Collection of all chess board legal moves
     */
    public Collection<Move> getAllLegalBoardMoves() {
        return Stream.concat(this.whitePlayer.getLegalMoves().stream(),
                this.blackPlayer.getLegalMoves().stream()).collect(Collectors.toList());
    }

    /***
     * Method to build normal chess board
     * @return Built standard board with pieces
     */
    public static Board createStandardBoard(){
        //setting black pieces
        BoardBuilder builder = new BoardBuilder();
        builder.setPiece(new Rook(0,Color.BLACK));
        builder.setPiece(new Knight(1,Color.BLACK));
        builder.setPiece(new Bishop(2,Color.BLACK));
        builder.setPiece(new Queen(3,Color.BLACK));
        builder.setPiece(new King(4,Color.BLACK));
        builder.setPiece(new Bishop(5,Color.BLACK));
        builder.setPiece(new Knight(6,Color.BLACK));
        builder.setPiece(new Rook(7,Color.BLACK));
        builder.setPiece(new Pawn(8,Color.BLACK));
        builder.setPiece(new Pawn(9,Color.BLACK));
        builder.setPiece(new Pawn(10,Color.BLACK));
        builder.setPiece(new Pawn(11,Color.BLACK));
        builder.setPiece(new Pawn(12,Color.BLACK));
        builder.setPiece(new Pawn(13,Color.BLACK));
        builder.setPiece(new Pawn(14,Color.BLACK));
        builder.setPiece(new Pawn(15,Color.BLACK));

        //setting white pieces
        builder.setPiece(new Rook(63,Color.WHITE));
        builder.setPiece(new Knight(62,Color.WHITE));
        builder.setPiece(new Bishop(61,Color.WHITE));
        builder.setPiece(new King(60,Color.WHITE));
        builder.setPiece(new Queen(59,Color.WHITE));
        builder.setPiece(new Bishop(58,Color.WHITE));
        builder.setPiece(new Knight(57,Color.WHITE));
        builder.setPiece(new Rook(56,Color.WHITE));
        builder.setPiece(new Pawn(55,Color.WHITE));
        builder.setPiece(new Pawn(54,Color.WHITE));
        builder.setPiece(new Pawn(53,Color.WHITE));
        builder.setPiece(new Pawn(52,Color.WHITE));
        builder.setPiece(new Pawn(51,Color.WHITE));
        builder.setPiece(new Pawn(50,Color.WHITE));
        builder.setPiece(new Pawn(49,Color.WHITE));
        builder.setPiece(new Pawn(48,Color.WHITE));

        //setting who have the first move
        builder.setColorNextMove(Color.WHITE);

        return builder.buildBoard();
    }

    /***
     * Get tile
     * @param tileCoords tile coords
     * @return Tile on coords
     */
    public Tile getTile(final int tileCoords) {
        return gamerBoard.get(tileCoords);
    }

    /***
     * get all black pieces
     * @return active black pieces
     */
    public Collection<Piece> getBlackPieces(){
        return this.blackPieces;
    }

    /***
     * Get all white pieces
     * @return active white pieces
     */
    public Collection<Piece> getWhitePieces(){
        return this.whitePieces;
    }

    /***
     * Get all legal moves
     * @return all legal moves on board
     */
    public Collection<Move> getAllLegalMoves(){
        return this.AllLegalsMoves;
    }

    /***
     * Representation of board in string
     * @return String representation of board
     */
    @Override
    public String toString(){
        final StringBuilder builder = new StringBuilder();
        for (int i = 0; i < BoardUtils.NumberOfTiles; i++){
            final String textOnTile = this.gamerBoard.get(i).toString();
            builder.append(String.format("%3s", textOnTile));
            if ((i + 1) % BoardUtils.NumberOfTilesPerRow == 0){
                builder.append("\n");
            }
        }
        return builder.toString();
    }
}
