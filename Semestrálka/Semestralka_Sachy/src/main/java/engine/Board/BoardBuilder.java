package engine.Board;

import engine.Color;
import engine.Pieces.Pawn;
import engine.Pieces.Piece;

import java.util.HashMap;
import java.util.Map;

/***
 * Class for building Board
 */
public class BoardBuilder {
    Map<Integer, Piece> boardMapping;
    Color colorNextMove;
    Pawn enPassantPawn;

    /***
     * Constructor for Board builder
     */
    public BoardBuilder(){
        this.boardMapping = new HashMap<>();
    }

    /***
     * Method for set a piece into empty TileMap
     * @param piece piece you want to set to tileMap
     * @return Board Builder with TileMap with set piece
     */
    public BoardBuilder setPiece(final Piece piece){
        this.boardMapping.put(piece.getPosition(),piece);
        return this;
    }
    public BoardBuilder setPieceOnCoords(final int coords, final Piece piece){
        this.boardMapping.put(coords,piece);
        return this;
    }

    /***
     * Method for set color of player for next move in the game
     * @param colorNextMove color which will have next move
     * @return Board Builder with set color for next move
     */
    public BoardBuilder setColorNextMove(final Color colorNextMove){
        this.colorNextMove = colorNextMove;
        return this;
    }

    /***
     * Method for building a Board you want to build
     * @return Built board
     */
    public Board buildBoard(){
        return new Board(this);
    }

    /***
     * Method to set En passant pawn
     * @param enPassantPawn which pawn you want to set
     */
    public void setEnPassantPawn(Pawn enPassantPawn) {
        this.enPassantPawn = enPassantPawn;
    }
}
