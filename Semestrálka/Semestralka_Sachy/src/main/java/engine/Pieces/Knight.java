package engine.Pieces;

import engine.Color;
import engine.Board.Board;
import engine.Board.BoardUtils;
import engine.Board.Move;
import engine.Board.Move.*;
import engine.Board.Tile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/***
 * Class for Knight piece
 */
public class Knight extends Piece{
    /***
     * Coords candidates for move
     */
    public final static int[] coordsCandidatesForMoves = {-17, -15, -10, -6, 6, 10, 15, 17};

    /***
     * Constructor for Knight
     * @param pieceCoords piece coordination
     * @param pieceColor piece color
     */
    public Knight(final int pieceCoords, final Color pieceColor) {
        super(pieceCoords, pieceColor, PiecesStringType.KNIGHT,true);
    }

    /***
     * Constructor for Knight
     * @param pieceCoords piece coordination
     * @param pieceColor piece color
     * @param isFirstMove Is it first move
     */
    public Knight(final int pieceCoords, final Color pieceColor, final boolean isFirstMove) {
        super(pieceCoords, pieceColor, PiecesStringType.KNIGHT, isFirstMove);
    }

    /***
     * Calculate all legal moves of selected Knight
     * @param board on which you want to calculate these moves
     * @return Collection of all legal moves of chosen piece
     */
    @Override
    public Collection<Move> calculateAllLegalMoves(final Board board) {
        final List<Move> legalMoves = new ArrayList<>();

        for (final int currentCandidate : coordsCandidatesForMoves){
            int candidateDestCoords = this.pieceCoords + currentCandidate;

            if (BoardUtils.isTileCoorsValid(candidateDestCoords)){
                if (FirstColumn(this.pieceCoords,currentCandidate) || SecondColumn(this.pieceCoords,currentCandidate)
                        || SeventhColumn(this.pieceCoords,currentCandidate) || EightColumn(this.pieceCoords,currentCandidate))
                {
                    continue;
                }

                final Tile candidateDestTile = board.getTile(candidateDestCoords);
                if (!candidateDestTile.isTileOccupied()){
                    legalMoves.add(new NormalMove(board, candidateDestCoords,this));
                }
                else {
                    final Piece pieceAtDestCoords = candidateDestTile.getPiece();
                    final Color pieceColor = pieceAtDestCoords.getPieceColor();

                    if (this.pieceColor != pieceColor){
                        legalMoves.add(new AttackMove(board,candidateDestCoords,this,pieceAtDestCoords));
                    }
                }
            }
        }
        return legalMoves;
    }

    /***
     * Method for moving Knight
     * @param move move you want to make
     * @return return moved Knight
     */
    @Override
    public Knight movePiece(Move move) {
        return new Knight(move.getDestCoords(),move.getMovedPiece().getPieceColor());
    }

    //special cases where it is not normal movement
    private static boolean FirstColumn(final int currentPosition, final int candidateCoords){
        return BoardUtils.FIRST_COLUMN[currentPosition] && (candidateCoords == -17  || candidateCoords == -10 || candidateCoords == 6 || candidateCoords == 15);
    }
    private static boolean SecondColumn(final int currentPosition, final int candidateCoords){
        return BoardUtils.SECOND_COLUMN[currentPosition] && (candidateCoords == -10 || candidateCoords == 6);
    }
    private static boolean SeventhColumn(final int currentPosition, final int candidateCoords){
        return BoardUtils.SEVENTH_COLUMN[currentPosition] && (candidateCoords == -6 || candidateCoords == 10);
    }
    private static boolean EightColumn(final int currentPosition, final int candidateCoords){
        return BoardUtils.EIGHT_COLUMN[currentPosition] && ((candidateCoords == -15  || candidateCoords == -6 || candidateCoords == 10 || candidateCoords == 17));
    }

    /***
     * Method to string
     * @return String representation
     */
    @Override
    public String toString() {
        return PiecesStringType.KNIGHT.toString();
    }
}
