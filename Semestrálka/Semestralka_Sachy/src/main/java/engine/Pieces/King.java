package engine.Pieces;

import engine.Board.BoardUtils;
import engine.Board.Tile;
import engine.Color;
import engine.Board.Board;
import engine.Board.Move;
import engine.Board.Move.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/***
 * Class for King piece
 */
public class King extends Piece {
    private final static int[] coordsCandidatesForMoves = {-9, -8, -7, -1, 1, 7, 8, 9};
    /***
     * Castles for king
     */
    public final Move[] castles = new Move[2];

    /***
     * Constructor for King
     * @param pieceCoords piece coordination
     * @param pieceColor piece color
     */
    public King(final int pieceCoords,final Color pieceColor) {
        super(pieceCoords, pieceColor, PiecesStringType.KING,true);
    }

    /***
     * Constructor for King
     * @param pieceCoords piece coordination
     * @param pieceColor piece color
     * @param isFirstMove Is it first move
     */
    public King(final int pieceCoords,final Color pieceColor,final boolean isFirstMove) {
        super(pieceCoords, pieceColor, PiecesStringType.KING,isFirstMove);
    }

    /***
     * Calculate all legal moves of selected King
     * @param board on which you want to calculate these moves
     * @return Collection of all legal moves of chosen piece
     */
    @Override
    public Collection<Move> calculateAllLegalMoves(Board board) {
        final List<Move> legalMoves = new ArrayList<>();

        if(castles[0] != null) legalMoves.add(castles[0]);
        if(castles[1] != null) legalMoves.add(castles[1]);

        for (final int currentCandidate : coordsCandidatesForMoves){
            int candidateDestCoords = this.pieceCoords + currentCandidate;
            if (BoardUtils.isTileCoorsValid(candidateDestCoords)){
                if (FirstColumn(this.pieceCoords,currentCandidate) ||  EightColumn(this.pieceCoords,currentCandidate))
                {
                    continue;
                }

                final Tile candidateDestTile = board.getTile(candidateDestCoords);
                //Adding normal move
                if (!candidateDestTile.isTileOccupied()){
                    legalMoves.add(new NormalMove(board, candidateDestCoords,this));
                }
                else {
                    final Piece pieceAtDestCoords = candidateDestTile.getPiece();
                    final Color pieceColor = pieceAtDestCoords.getPieceColor();
                    //Adding attacking move bcs color is not same
                    if (this.pieceColor != pieceColor){
                        legalMoves.add(new AttackMove(board,candidateDestCoords,this,pieceAtDestCoords));
                    }
                }
            }

        }
        return legalMoves;
    }

    /***
     * Method for moving King
     * @param move move you want to make
     * @return return moved King
     */
    @Override
    public King movePiece(Move move) {
        return new King(move.getDestCoords(),move.getMovedPiece().getPieceColor(), false);
    }

    //special cases where it is not normal movement
    private static boolean FirstColumn(final int currentPosition, final int candidateCoords){
        return BoardUtils.FIRST_COLUMN[currentPosition] && (candidateCoords == -9  || candidateCoords == -1 || candidateCoords == 7);
    }
    private static boolean EightColumn(final int currentPosition, final int candidateCoords){
        return BoardUtils.EIGHT_COLUMN[currentPosition] && (candidateCoords == -7 || candidateCoords == 1 || candidateCoords == 9);
    }

    /***
     * Method to string
     * @return String representation
     */
    @Override
    public String toString() {
        return PiecesStringType.KING.toString();
    }
}
