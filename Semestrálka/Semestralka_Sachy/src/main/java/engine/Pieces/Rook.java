package engine.Pieces;

import engine.Color;
import engine.Board.Board;
import engine.Board.BoardUtils;
import engine.Board.Move;
import engine.Board.Move.*;
import engine.Board.Tile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/***
 * Class for Rook piece
 */
public class Rook extends Piece {

    private final static int[] coordsCandidatesForMoves = {-8, -1, 1, 8};
    /***
     * Constructor for Rook
     * @param pieceCoords piece coordination
     * @param pieceColor piece color
     */
    public Rook(final int pieceCoords,final Color pieceColor) {
        super(pieceCoords, pieceColor, PiecesStringType.ROOK,true);
    }
    /***
     * Constructor for Rook
     * @param pieceCoords piece coordination
     * @param pieceColor piece color
     * @param isFirstMove Is it first move
     */
    public Rook(final int pieceCoords,final Color pieceColor,final boolean isFirstMove) {
        super(pieceCoords, pieceColor, PiecesStringType.ROOK,isFirstMove);
    }

    /***
     * Calculate all legal moves of selected Rook
     * @param board on which you want to calculate these moves
     * @return Collection of all legal moves of chosen piece
     */
    @Override
    public Collection<Move> calculateAllLegalMoves(final Board board) {
        final List<Move> legalMoves = new ArrayList<>();

        for (final int currentCandidate: coordsCandidatesForMoves){

            int candidateDestCoords = this.pieceCoords;

            while (BoardUtils.isTileCoorsValid(candidateDestCoords)){

                if (firstColumn(candidateDestCoords,currentCandidate) || eightColumn(candidateDestCoords,currentCandidate)){
                    break;
                }

                candidateDestCoords += currentCandidate;

                if (BoardUtils.isTileCoorsValid(candidateDestCoords)){
                    final Tile candidateDestTile = board.getTile(candidateDestCoords);

                    if (!candidateDestTile.isTileOccupied()){
                        legalMoves.add(new NormalMove(board, candidateDestCoords,this));
                    }
                    else {
                        final Piece pieceAtDestCoords = candidateDestTile.getPiece();
                        final Color pieceColor = pieceAtDestCoords.getPieceColor();

                        if (this.pieceColor != pieceColor){
                            legalMoves.add(new AttackMove(board,candidateDestCoords,this,pieceAtDestCoords));
                        }
                        break;
                    }
                }
            }
        }
        return legalMoves;
    }
    /***
     * Method for moving Rook
     * @param move move you want to make
     * @return return moved Rook
     */
    @Override
    public Rook movePiece(Move move) {
        return new Rook(move.getDestCoords(),move.getMovedPiece().getPieceColor());
    }

    //special cases where it is not normal movement
    private static boolean firstColumn(final int currentPosition, final int candidateCoords){
        return BoardUtils.FIRST_COLUMN[currentPosition] && (candidateCoords == -1);
    }
    private static boolean eightColumn(final int currentPosition, final int candidateCoords){
        return BoardUtils.EIGHT_COLUMN[currentPosition] && (candidateCoords == 1);
    }
    /***
     * Method to string
     * @return String representation
     */
    @Override
    public String toString() {
        return PiecesStringType.ROOK.toString();
    }

}
