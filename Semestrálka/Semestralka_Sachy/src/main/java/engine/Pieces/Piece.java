package engine.Pieces;

import engine.Color;
import engine.Board.Board;
import engine.Board.Move;

import java.util.Collection;

/***
 * Abstract class for piece
 */
public abstract class Piece {

    protected int pieceCoords;
    protected final Color pieceColor;
    protected boolean isFirstMove;
    protected final PiecesStringType piecesStringType;
    private final int cachedHashCode;

    /***
     * Constructor for Piece class
     * @param pieceCoords piece coordination
     * @param pieceColor piece color
     * @param piecesStringType string representation
     * @param isFirstMove boolean for first move
     */
    public Piece(final int pieceCoords,final Color pieceColor,final PiecesStringType piecesStringType, final boolean isFirstMove) {
        this.pieceCoords = pieceCoords;
        this.pieceColor = pieceColor;
        this.piecesStringType = piecesStringType;
        this.isFirstMove = isFirstMove;
        this.cachedHashCode = computeHashCode();
    }

    /***
     * Equals method
     * @param other other item
     * @return boolean
     */
    @Override
    public boolean equals(final Object other){
        if (this == other){
            return true;
        }
        if (!(other instanceof Piece)){
            return false;
        }
        final Piece otherPiece = (Piece) other;

        return pieceCoords == otherPiece.getPosition() && piecesStringType == otherPiece.getPiecesStringType() &&
                pieceColor == otherPiece.getPieceColor() && isFirstMove == otherPiece.isFirstMove();
    }

    private int computeHashCode(){
        int result = piecesStringType.hashCode();
        result = 31 * result + pieceColor.hashCode();
        result = 31 * result + pieceCoords;
        result = 31 * result + (isFirstMove ? 1 : 0);
        return  result;
    }

    /***
     * Hashcode representation
     * @return hashcode representation
     */
    @Override
    public int hashCode(){
        return this.cachedHashCode;
    }

    /***
     * Get string representation of piece
     * @return string representation of piece
     */
    public PiecesStringType getPiecesStringType(){
        return this.piecesStringType;
    }

    /***
     * Getter for piece value
     * @return piece value
     */
    public int getPieceValue() {
        return this.piecesStringType.getPieceValue();
    }

    /***
     * Get Position method
     * @return piece coords
     */
    public int getPosition() {
        return this.pieceCoords;
    }

    /***
     * Is first move method
     * @return boolean isFirstMove
     */
    public boolean isFirstMove() {
        return this.isFirstMove;
    }

    /***
     * Abstract method to calculate legal moves
     * @param board What I want to use
     * @return Collection of legal moves
     */
    public abstract Collection<Move> calculateAllLegalMoves(final Board board);

    /***
     * Get Piece Color
     * @return Piece Color
     */
    public Color getPieceColor(){
        return this.pieceColor;
    }

    /***
     * Abstract method for moving piece
     * @param move move u want to make
     * @return moved piece
     */
    public abstract Piece movePiece(Move move);


}
