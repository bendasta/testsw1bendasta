package engine.Pieces;

import engine.Color;
import engine.Board.Board;
import engine.Board.BoardUtils;
import engine.Board.Move;
import engine.Board.Move.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/***
 * Class for Pawn piece
 */
public class Pawn extends Piece {
    private final static int[] coordsCandidatesForMoves = {7 ,8 ,9 ,16};
    private final int oneForward = 8;

    /***
     * Constructor for Pawn
     * @param pieceCoords piece coordination
     * @param pieceColor piece color
     */
    public Pawn(final int pieceCoords,final Color pieceColor) {
        super(pieceCoords, pieceColor, PiecesStringType.PAWN,true);
    }
    /***
     * Constructor for Pawn
     * @param pieceCoords piece coordination
     * @param pieceColor piece color
     * @param isFirstMove Is it first move
     */
    public Pawn(final int pieceCoords,final Color pieceColor,final boolean isFirstMove) {
        super(pieceCoords, pieceColor, PiecesStringType.PAWN,isFirstMove);
    }

    /***
     * Calculate all legal moves of selected Pawn
     * @param board on which you want to calculate these moves
     * @return Collection of all legal moves of chosen piece
     */
    @Override
    public Collection<Move> calculateAllLegalMoves(final Board board) {
        final List<Move> legalMoves = new ArrayList<>();

        for (final int currentCandidate : coordsCandidatesForMoves){
            int candidateDestCoords = this.pieceCoords + (this.getPieceColor().getDirection() * currentCandidate);

            if (!BoardUtils.isTileCoorsValid(candidateDestCoords)){
                continue;
            }
            if (currentCandidate == 8 && !board.getTile(candidateDestCoords).isTileOccupied()){
                if (this.pieceColor.isPawnPromoted(candidateDestCoords)){
                    legalMoves.add(new PawnPromotion(new PawnMove(board,candidateDestCoords,this)));
                }
                else {
                    legalMoves.add(new PawnMove(board,candidateDestCoords,this));
                }
            }

            //Check if it wants to move forward for 2 tiles, and it's blackPawn it's on seventh row (starting position) and if it's whitePawn it's on second row (starting position)
            else if(currentCandidate == 16 && this.isFirstMove() &&
                    ((BoardUtils.SECOND_ROW[this.pieceCoords] && this.getPieceColor().isBlack()) || (BoardUtils.SEVENTH_ROW[this.pieceCoords] && this.getPieceColor().isWhite()))){

                //First move - double jump - first tile is not occupied
                final int beforeDestCoords = this.pieceCoords + (this.pieceColor.getDirection() * oneForward);
                //If neither of those tiles are not occupied then add not attacking move
                if (!board.getTile(beforeDestCoords).isTileOccupied() && !board.getTile(candidateDestCoords).isTileOccupied()){
                    legalMoves.add(new PawnJump(board,candidateDestCoords,this));
                }
            }
            else if (currentCandidate == 7 && !((BoardUtils.EIGHT_COLUMN[this.pieceCoords] && this.pieceColor.isWhite() ||
                    (BoardUtils.FIRST_COLUMN[this.pieceCoords] && this.pieceColor.isBlack())))){
                if (board.getTile(candidateDestCoords).isTileOccupied()){
                    final Piece pieceOnDestination = board.getTile(candidateDestCoords).getPiece();
                    if ( this.pieceColor != pieceOnDestination.getPieceColor()){
                        if (this.pieceColor.isPawnPromoted(candidateDestCoords)){
                            legalMoves.add(new PawnPromotion(new PawnAttackMove(board,candidateDestCoords,this,pieceOnDestination)));
                        }
                        else {
                            legalMoves.add(new PawnAttackMove(board,candidateDestCoords,this,pieceOnDestination));
                        }
                    }
                }
                else if (board.getEnPassantPawn() != null){
                    if (board.getEnPassantPawn().getPosition() == (this.pieceCoords + (this.pieceColor.getOppositeDirection()))){
                        final Piece pieceOnCandidate = board.getEnPassantPawn();
                        if (this.pieceColor != pieceOnCandidate.getPieceColor()){
                            legalMoves.add(new PawnEnPassantMove(board,candidateDestCoords,this,pieceOnCandidate));
                        }
                    }
                }
            }
            else if (currentCandidate == 9 && !((BoardUtils.FIRST_COLUMN[this.pieceCoords] && this.pieceColor.isWhite() ||
                    (BoardUtils.EIGHT_COLUMN[this.pieceCoords] && this.pieceColor.isBlack())))){
                if (board.getTile(candidateDestCoords).isTileOccupied()){
                    final Piece pieceOnDestination = board.getTile(candidateDestCoords).getPiece();
                    if ( this.pieceColor != pieceOnDestination.getPieceColor()){
                        if (this.pieceColor.isPawnPromoted(candidateDestCoords)){
                            legalMoves.add(new PawnPromotion(new PawnAttackMove(board,candidateDestCoords,this,pieceOnDestination)));
                        }
                        else {
                            legalMoves.add(new PawnAttackMove(board, candidateDestCoords, this, pieceOnDestination));
                        }
                    }
                }
                else if (board.getEnPassantPawn() != null){
                    if (board.getEnPassantPawn().getPosition() == (this.pieceCoords - (this.pieceColor.getOppositeDirection()))){
                        final Piece pieceOnCandidate = board.getEnPassantPawn();
                        if (this.pieceColor != pieceOnCandidate.getPieceColor()){
                            legalMoves.add(new PawnEnPassantMove(board,candidateDestCoords,this,pieceOnCandidate));
                        }
                    }
                }
            }
        }
        return legalMoves;
    }

    /***
     * Method for moving Pawn
     * @param move move you want to make
     * @return return moved Pawn
     */
    @Override
    public Pawn movePiece(Move move) {
        return new Pawn(move.getDestCoords(),move.getMovedPiece().getPieceColor());
    }

    /***
     * Method to string
     * @return String representation
     */
    @Override
    public String toString() {
        return PiecesStringType.PAWN.toString();
    }

    /***
     * Method for pawn promotion
     * @return Promoted piece
     */
    public Piece getPromotionPiece(){
        return new Queen(this.pieceCoords,this.pieceColor,false);
    }
}
