package engine.Pieces;

/***
 * String type enum
 */
public enum PiecesStringType {
    /***
     * Pawn representation
     */
    PAWN(100,"P"){
        @Override
        public boolean isRook() {
            return false;
        }

        @Override
        public boolean isKing() {
            return false;
        }
    },
    /***
     * Knight representation
     */
    KNIGHT(300,"N") {
        @Override
        public boolean isRook() {
            return false;
        }

        @Override
        public boolean isKing() {
            return false;
        }
    },
    /***
     * Bishop representation
     */
    BISHOP(300,"B") {
        @Override
        public boolean isRook() {
            return false;
        }

        @Override
        public boolean isKing() {
            return false;
        }
    },
    /***
     * Rook representation
     */
    ROOK(500,"R") {
        @Override
        public boolean isRook() {
            return true;
        }

        @Override
        public boolean isKing() {
            return false;
        }
    },
    /***
     * Queen representation
     */
    QUEEN(900,"Q") {
        @Override
        public boolean isRook() {
            return false;
        }

        @Override
        public boolean isKing() {
            return false;
        }
    },
    /***
     * King representation
     */
    KING(1000,"K") {
        @Override
        public boolean isRook() {
            return false;
        }

        @Override
        public boolean isKing() {
            return true;
        }
    };

    private String pieceName;
    private int pieceValue;

    PiecesStringType(final int pieceValue,final String pieceName) {
        this.pieceName = pieceName;
        this.pieceValue = pieceValue;
    }

    /***
     * String representation of piece
     * @return String
     */
    @Override
    public String toString() {
        return this.pieceName;
    }

    /***
     * Is rook
     * @return boolean
     */
    public abstract boolean isRook();

    /***
     * Is King
     * @return boolean
     */
    public abstract boolean isKing();

    /***
     * Piece value getter
     * @return value of piece
     */
    public int getPieceValue() {
        return this.pieceValue;
    }
}
