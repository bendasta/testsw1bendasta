package engine;

import GUI.Table;
import engine.Board.Board;

/***
 * Application class
 */
public class Chess {
    /***
     * Method for starting chess
     * @param args arguments
     */
    public static void main(String[] args) {
        Board board = Board.createStandardBoard();
        System.out.println(board);

        Table.get().show();
    }
}
