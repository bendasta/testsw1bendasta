package engine;

import engine.Board.BoardUtils;
import engine.Player.BlackPlayer;
import engine.Player.Player;
import engine.Player.WhitePlayer;

/***
 *
 */
public enum Color {
    /***
     * Direction for Black pieces, some getters
     */
    BLACK{
        @Override
        public int getDirection(){
            return 1;
        }

        @Override
        public boolean isBlack() {
            return true;
        }

        @Override
        public boolean isWhite() {
            return false;
        }

        @Override
        public Player chosePlayer(WhitePlayer whitePlayer, BlackPlayer blackPlayer) {
            return blackPlayer;
        }

        @Override
        public int getOppositeDirection() {
            return -1;
        }

        @Override
        public boolean isPawnPromoted(int position) {
            return BoardUtils.EIGHT_ROW[position];
        }
    },
    /***
     * Direction for White pieces, some getters
     */
    WHITE{
        @Override
        public int getDirection(){
            return -1;
        }

        @Override
        public boolean isBlack() {
            return false;
        }

        @Override
        public boolean isWhite() {
            return true;
        }

        @Override
        public Player chosePlayer(WhitePlayer whitePlayer, BlackPlayer blackPlayer) {
            return whitePlayer;
        }

        @Override
        public int getOppositeDirection() {
            return 1;
        }

        @Override
        public boolean isPawnPromoted(int position) {
            return BoardUtils.FIRST_ROW[position];
        }
    };

    /***
     * Method for getting piece direction
     * @return Direction of piece
     */
    public abstract int getDirection();

    /***
     * Method for checking if piece is black
     * @return boolean
     */
    public abstract boolean isBlack();

    /***
     * Method for checking if piece is white
     * @return boolean
     */
    public abstract boolean isWhite();

    /***
     * Method to chose player for next move
     * @param whitePlayer White player
     * @param blackPlayer Black player
     * @return Player who is on move right now
     */
    public abstract Player chosePlayer(WhitePlayer whitePlayer, BlackPlayer blackPlayer);

    /***
     * Method for getting opposite direction
     * @return Opposite direction
     */
    public abstract int getOppositeDirection();

    /***
     * Method for checking if Pawn is promoted
     * @param position Pawn coords
     * @return boolean
     */
    public abstract boolean isPawnPromoted(int position);
}
