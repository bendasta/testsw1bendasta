package engine.Player;

import engine.Board.Board;
import engine.Board.BoardUtils;
import engine.Board.Move;
import engine.Color;
import engine.Pieces.King;
import engine.Pieces.Piece;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/***
 * Abstract class for player
 */
public abstract class Player {

    protected Board board;
    protected King playerKing;
    protected Collection<Move> legalMoves = new ArrayList<>();
    private final boolean isInCheck;
    final static Logger logger = Logger.getLogger(Move.class.getName());

    /***
     * Constructor for player class
     * @param board Current board
     * @param playerLegals current player legal moves
     * @param opponentMoves opponent player legal moves
     */
    public Player(Board board, Collection<Move> playerLegals, Collection<Move> opponentMoves) {
        this.board = board;
        this.playerKing = establishKing();
        this.isInCheck = !Player.calculateAttacksOnTile(this.playerKing.getPosition(),opponentMoves).isEmpty();
        this.legalMoves.addAll(calculateCastle(legalMoves,opponentMoves));
        this.legalMoves.addAll(playerLegals);
    }

    /***
     * Method to calculate attack on specific target - here it is king
     * @param pieceCoords coords of tile where you want to attack
     * @param moves Collection of opponent moves
     * @return Collection of attacks on your piece
     */
    protected static Collection<Move> calculateAttacksOnTile(int pieceCoords, Collection<Move> moves) {
        final List<Move> attackMoves = new ArrayList<>();
        for(final Move move: moves){
            if (pieceCoords == move.getDestCoords()){
                attackMoves.add(move);
            }
        }
        return attackMoves;
    }

    /***
     * Method for getting current player King
     * @return King
     */
    public King getPlayerKing() {
        return this.playerKing;
    }

    /***
     * Method for getting legal moves
     * @return Collection of legal moves
     */
    public Collection<Move> getLegalMoves() {
        return this.legalMoves;
    }

    /***
     * Checking if it is legal move
     * @param move Move you want to check
     * @return boolean
     */
    public boolean isMoveLegal(final Move move){
        return this.legalMoves.contains(move);
    }

    /***
     * Method for checking if player is in check
     * @return boolean
     */
    public boolean isInCheck(){
        return this.isInCheck;
    }

    /***
     * Method for checking if player is in check mate
     * @return boolean
     */
    public boolean isInCheckMate(){
        return this.isInCheck && !hasEscapeMoves();
    }

    protected boolean hasEscapeMoves() {
        for (final Move move : this.legalMoves){
            final MoveTransition transition = makeMove(move);
            if (transition.getMoveStatus().isDone()){
                return true;
            }
        }
        return false;
    }

    /***
     * Method to check if there is a king on board and if there is not it will throw exeption
     * @return King piece or throw exception
     */
    private King establishKing(){
        for (final Piece piece : getAllActivePieces()){
            if (piece.getPiecesStringType().isKing()){
                return (King) piece;
            }
        }
        throw new RuntimeException("Not valid board!!!!");
    }

    /***
     * Method to make move you chose to make
     * @param move you want to make
     * @return Move transition with new board where is move done
     */
    public MoveTransition makeMove(final Move move){
        if (!isMoveLegal(move)){
            return new MoveTransition(this.board, move, MoveStatus.IllegalMove);
        }
        final Board transitionBoard = move.execute();
        logger.info("Piece " + move.getMovedPiece().toString() + " from starting position: " + move.getMovedPiece().getPosition() + " To position " + move.getDestCoords());
        return transitionBoard.currentPlayer().getOpponent().isInCheck() ?
                new MoveTransition(this.board, move, MoveStatus.PlayerInCheck) :
                new MoveTransition(transitionBoard, move, MoveStatus.Done);
    }

    /***
     * Method for getting all active pieces on board
     * @return Collection of all active pieces
     */
    public abstract Collection<Piece> getAllActivePieces();

    /***
     * Method for getting piece color
     * @return piece color
     */
    public abstract Color getColor();

    /***
     * Method for getting opponent
     * @return opponent player
     */
    public abstract Player getOpponent();

    /***
     * Method for calculating castles
     * @param playerLegalMoves Current player legal moves
     * @param opponentsLegalMoves Opponent legal moves
     * @return Collection of castling moves
     */
    public abstract Collection<Move> calculateCastle(Collection<Move> playerLegalMoves, Collection<Move> opponentsLegalMoves);

}
