package engine.Player;

import engine.Board.Board;
import engine.Board.Move;
import engine.Board.Move.*;
import engine.Board.Tile;
import engine.Color;
import engine.Pieces.Piece;
import engine.Pieces.Rook;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/***
 * Class for White player
 */
public class WhitePlayer extends Player {
    /***
     * Constructor for White player class
     * @param board Current board
     * @param whiteNormalMoves All white moves
     * @param blackNormalMoves All black moves
     */
    public WhitePlayer(final Board board,final Collection<Move> whiteNormalMoves,final  Collection<Move> blackNormalMoves) {
        super(board,whiteNormalMoves,blackNormalMoves);
    }

    /***
     * Get all White active pieces on board
     * @return Collection of White active pieces
     */
    @Override
    public Collection<Piece> getAllActivePieces() {
        return this.board.getWhitePieces();
    }

    /***
     * Method for getting piece color
     * @return piece color
     */
    @Override
    public Color getColor() {
        return Color.WHITE;
    }

    /***
     * Method for getting opponent
     * @return opponent player
     */
    @Override
    public Player getOpponent() {
        return this.board.getBlackPlayer();
    }

    /***
     * Calculate all castling moves you can make as a White player
     * @param playerLegalMoves Collection of legal move for White player
     * @param opponentsLegalMoves Collection of legal move for opponent player
     * @return Collection of calculated castling moves u can make
     */
    @Override
    public Collection<Move> calculateCastle(final Collection<Move> playerLegalMoves,final Collection<Move> opponentsLegalMoves) {
        final List<Move> castles = new ArrayList<>();
        boolean added = false;
        boolean added2 = false;

        if(this.playerKing.isFirstMove() && !this.isInCheck()){
            //white king side castle
            if (!this.board.getTile(61).isTileOccupied() && !this.board.getTile(62).isTileOccupied()){
                final Tile rookTile = this.board.getTile(63);

                if (rookTile.isTileOccupied() && rookTile.getPiece().isFirstMove()){
                    if (Player.calculateAttacksOnTile(61,opponentsLegalMoves).isEmpty() &&
                            Player.calculateAttacksOnTile(62,opponentsLegalMoves).isEmpty() && rookTile.getPiece().getPiecesStringType().isRook()){
                        KingSideCastleMove move = new KingSideCastleMove(this.board,62,this.playerKing,(Rook) rookTile.getPiece(),rookTile.getTileCoords(),61);
                        playerKing.castles[0] = move;
                        castles.add(move);
                        added = true;
                    }
                }
            }
            //white queen side castle
            if (!this.board.getTile(59).isTileOccupied() && !this.board.getTile(58).isTileOccupied() && !this.board.getTile(57).isTileOccupied()){
                final  Tile rookTile = this.board.getTile(56);

                if (rookTile.isTileOccupied() && rookTile.getPiece().isFirstMove()){
                    QueenSideCastleMove move = new QueenSideCastleMove(this.board,58,this.playerKing,(Rook) rookTile.getPiece(),rookTile.getTileCoords(),59);
                    playerKing.castles[1] = move;
                    castles.add(move);
                    added2 = true;
                }
            }
        }
        if(!added) playerKing.castles[0] = null;
        if(!added2) playerKing.castles[1] = null;
        return castles;
    }
}
