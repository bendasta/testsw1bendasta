package engine.Player;

/***
 * Enum for transition
 */
public enum MoveStatus {
    /***
     * Move is done
     */
    Done{
        /***
         * Method for knowledge if move is done
         * @return Boolean (True)
         */
        @Override
        public boolean isDone(){
            return true;
        }
    },
    /***
     * Move is illegal
     */
    IllegalMove{
        /***
         * Method for knowledge if move is done
         * @return Boolean (False)
         */
        @Override
        public boolean isDone() {
            return false;
        }
    },
    /***
     * Player is in check
     */
    PlayerInCheck{
        /***
         * Method for knowledge if move is done
         * @return Boolean (False)
         */
        @Override
        public boolean isDone() {
            return false;
        }
    };

    /***
     * Method for knowledge if move was done properly
     * @return Boolean
     */
    public abstract boolean isDone();
}
