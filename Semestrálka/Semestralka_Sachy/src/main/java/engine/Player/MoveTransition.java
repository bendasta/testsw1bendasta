package engine.Player;

import engine.Board.Board;
import engine.Board.Move;

/***
 * Class for move transition
 */
public class MoveTransition {
    private final Board transitionBoard;
    private final Move move;
    private final MoveStatus moveStatus;

    /***
     * Constructor for Move transition class
     * @param transitionBoard board for transition
     * @param move move you want to make
     * @param moveStatus status of move
     */
    public MoveTransition(Board transitionBoard, Move move, MoveStatus moveStatus) {
        this.transitionBoard = transitionBoard;
        this.move = move;
        this.moveStatus = moveStatus;
    }

    /***
     * Method for getting move status
     * @return Move status
     */
    public MoveStatus getMoveStatus() {
        return this.moveStatus;
    }

    /***
     * Method for getting transition board
     * @return Transition board
     */
    public Board getTransitionBoard() {
        return this.transitionBoard;
    }
}
