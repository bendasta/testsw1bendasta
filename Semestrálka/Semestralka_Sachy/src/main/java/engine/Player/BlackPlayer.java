package engine.Player;

import engine.Board.Board;
import engine.Board.Move;
import engine.Board.Move.*;
import engine.Board.Tile;
import engine.Color;
import engine.Pieces.Piece;
import engine.Pieces.Rook;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/***
 * Class for Black player
 */
public class BlackPlayer extends Player {
    /***
     * Constructor for Black player class
     * @param board Current board
     * @param whiteNormalMoves All white moves
     * @param blackNormalMoves All black moves
     */
    public BlackPlayer(final Board board, final Collection<Move> whiteNormalMoves,final Collection<Move> blackNormalMoves) {
        super(board,whiteNormalMoves,blackNormalMoves);
    }

    /***
     * Get all Black active pieces on board
     * @return Collection of Black active pieces
     */
    @Override
    public Collection<Piece> getAllActivePieces() {
        return this.board.getBlackPieces();
    }

    /***
     * Method for getting piece color
     * @return piece color
     */
    @Override
    public Color getColor() {
        return Color.BLACK;
    }

    /***
     * Method for getting opponent
     * @return opponent player
     */
    @Override
    public Player getOpponent() {
        return this.board.getWhitePlayer();
    }

    /***
     * Calculate all castling moves you can make as a Black player
     * @param playerLegalMoves Collection of legal move for Black player
     * @param opponentsLegalMoves Collection of legal move for opponent player
     * @return Collection of calculated castling moves u can make
     */
    @Override
    public Collection<Move> calculateCastle(final Collection<Move> playerLegalMoves,final Collection<Move> opponentsLegalMoves) {
        final List<Move> castles = new ArrayList<>();
        boolean added = false;
        boolean added2 = false;
        if(this.playerKing.isFirstMove() && !this.isInCheck()){
            //black king side castle
            if (!this.board.getTile(5).isTileOccupied() && !this.board.getTile(6).isTileOccupied()){
                final Tile rookTile = this.board.getTile(7);

                if (rookTile.isTileOccupied() && rookTile.getPiece().isFirstMove()){
                    if (Player.calculateAttacksOnTile(5,opponentsLegalMoves).isEmpty() &&
                            Player.calculateAttacksOnTile(6,opponentsLegalMoves).isEmpty() && rookTile.getPiece().getPiecesStringType().isRook()){
                        KingSideCastleMove move = new KingSideCastleMove(this.board,6,this.playerKing,(Rook) rookTile.getPiece(),rookTile.getTileCoords(),5);
                        playerKing.castles[0] = move;
                        castles.add(move);
                        added = true;
                    }
                }
            }
            //black queen side castle
            if (!this.board.getTile(1).isTileOccupied() && !this.board.getTile(2).isTileOccupied() && !this.board.getTile(3).isTileOccupied()){
                final  Tile rookTile = this.board.getTile(0);

                if (rookTile.isTileOccupied() && rookTile.getPiece().isFirstMove()){
                    QueenSideCastleMove move = new QueenSideCastleMove(this.board,2, this.playerKing,(Rook) rookTile.getPiece(),rookTile.getTileCoords(),3);
                    playerKing.castles[1] = move;
                    castles.add(move);
                    added2 = true;
                }
            }
        }
        if(!added) playerKing.castles[0] = null;
        if(!added2) playerKing.castles[1] = null;
        return castles;
    }
}
