package engine.Player.AI;

import engine.Board.Board;
import engine.Pieces.Piece;
import engine.Player.Player;

/***
 * Class for standard board evaluation
 */
public final class StandardBoardEval implements BoardEvaluator {

    private static final int checkBonus = 50;
    private static final int checkMateBonus = 5000;

    @Override
    public int evaluate(Board board, int depth) {
        return scorePlayer(board,board.getWhitePlayer(),depth) - scorePlayer(board,board.getBlackPlayer(),depth);
    }

    private int scorePlayer(final Board board,final Player player,final int depth) {
        return pieceValue(player) + mobility(player) + check(player) + checkMate(player, depth);
    }


    private static int checkMate(Player player, int depth) {
        return  player.getOpponent().isInCheckMate() ? checkMateBonus * depthBonus(depth) : 0;
    }

    private static int depthBonus(int depth) {
        return depth == 0 ? 1 : 100 * depth;
    }

    private static int check(Player player) {
        return player.getOpponent().isInCheck() ? checkBonus : 0;
    }

    private static int mobility(Player player) {
        return player.getLegalMoves().size();
    }

    private int pieceValue(final Player player) {
        int pieceValue = 0;
        for (final Piece piece : player.getAllActivePieces()){
            pieceValue += piece.getPieceValue();
        }
        return pieceValue;
    }
}
