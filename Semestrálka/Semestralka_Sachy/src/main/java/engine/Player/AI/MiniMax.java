package engine.Player.AI;

import engine.Board.Board;
import engine.Board.Move;
import engine.Player.MoveTransition;

/***
 * Move strategy MiniMax
 */
public class MiniMax implements MoveStrategy{
    private final BoardEvaluator boardEvaluator;
    private final int searchDepth;

    /***
     * Constructor for class
     * @param searchDepth search depth you want to search move
     */
    public MiniMax(final int searchDepth){
        this.boardEvaluator = new StandardBoardEval();
        this.searchDepth = searchDepth;
    }
    @Override
    public String toString(){
        return "MiniMax";
    }
    @Override
    public Move execute(Board board) {
        final long sTime = System.currentTimeMillis();
        Move bestMove = null;

        int highestVal = Integer.MIN_VALUE;
        int lowestVal = Integer.MAX_VALUE;
        int currentVal;
        int numMoves = board.currentPlayer().getLegalMoves().size();

        for (final Move move : board.currentPlayer().getLegalMoves()){
            final MoveTransition moveTransition = board.currentPlayer().makeMove(move);
            if (moveTransition.getMoveStatus().isDone()){
                currentVal = board.currentPlayer().getColor().isWhite() ? min(moveTransition.getTransitionBoard(),this.searchDepth-1):
                        max(moveTransition.getTransitionBoard(),this.searchDepth-1);


                if (board.currentPlayer().getColor().isWhite() && currentVal >= highestVal){
                    highestVal = currentVal;
                    bestMove = move;
                }
                else if (board.currentPlayer().getColor().isBlack() && currentVal <= lowestVal){
                    lowestVal = currentVal;
                    bestMove = move;
                }
            }
        }
        final long execTime = System.currentTimeMillis() - sTime;
        return bestMove;
    }
    private static boolean endGame(final Board board){
        return board.currentPlayer().isInCheckMate();
    }

    /***
     * Method for calculating min value
     * @param board Board you want to use
     * @param depth Depth you want to calculate
     * @return Min value
     */
    public int min(final Board board,final int depth){
        if (depth == 0 || endGame(board)){
            return this.boardEvaluator.evaluate(board,depth);
        }
        int lowestVal =  Integer.MAX_VALUE;
        for (final Move move : board.currentPlayer().getLegalMoves()){
            final MoveTransition moveTransition = board.currentPlayer().makeMove(move);
            if (moveTransition.getMoveStatus().isDone()){
                final int currentVal = max(moveTransition.getTransitionBoard(), depth-1);
                if (currentVal <= lowestVal){
                    lowestVal = currentVal;
                }
            }
        }
        return lowestVal;
    }

    /***
     * Method for calculating max value
     * @param board Board you want to use
     * @param depth Depth you want to calculate
     * @return Max value
     */
    public int max(final Board board,final int depth){
        if (depth == 0 || endGame(board)){
            return this.boardEvaluator.evaluate(board,depth);
        }
        int highestVal =  Integer.MIN_VALUE;
        for (final Move move : board.currentPlayer().getLegalMoves()){
            final MoveTransition moveTransition = board.currentPlayer().makeMove(move);
            if (moveTransition.getMoveStatus().isDone()){
                final int currentVal = min(moveTransition.getTransitionBoard(), depth-1);
                if (currentVal >= highestVal){
                    highestVal = currentVal;
                }
            }
        }
        return highestVal;
    }
}
