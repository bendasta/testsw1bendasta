package engine.Player.AI;

import engine.Board.Board;
import engine.Board.Move;

/***
 * Interface move strategy
 */
public interface MoveStrategy {
    /***
     * @param board board on which you want to exec it
     * @return Exec. move
     */
    Move execute(Board board);
}
