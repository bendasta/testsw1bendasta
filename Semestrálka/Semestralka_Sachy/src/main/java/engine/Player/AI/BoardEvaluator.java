package engine.Player.AI;

import engine.Board.Board;

/***
 * Interface for board evaluation for AI
 */
public interface BoardEvaluator {
    /***
     * @param board Board you want to evaluate
     * @param depth depth
     * @return Int
     */
    int evaluate(Board board, int depth);
}
