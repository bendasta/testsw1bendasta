import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
    Calculator calculator = new Calculator();
    int a = 0;
    int b = 0;
    @Test
    public void AddingTest(){
        a = 1;
        b = 2;
        int expectedValue = 3;
        int result = calculator.scitani(a,b);

        Assertions.assertEquals(expectedValue, result);
    }
    @Test
    public void substractionTest(){
        a = 2;
        b = 1;
        int expectedValue = 1;
        int result = calculator.odcitani(a,b);

        Assertions.assertEquals(expectedValue,result);
    }
    @Test
    public void multiplicationTest(){
        a = 2;
        b = 1;
        int expectedValue = 2;
        int result = calculator.nasobeni(a,b);

        Assertions.assertEquals(expectedValue,result);
    }
    @Test
    public void divisionTest(){
        a = 6;
        b = 2;
        int expectedValue = 3;
        int result = calculator.deleni(a,b);

        Assertions.assertEquals(expectedValue,result);
    }
    @Test
    public void dividedByZeroTest(){
        a = 4;
        b = 0;

        Assertions.assertThrows(ArithmeticException.class, () -> calculator.deleni(a,b));
    }
}
