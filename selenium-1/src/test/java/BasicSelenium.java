import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BasicSelenium {
    WebDriver driver;

    @BeforeEach
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","/import/users/bendasta/CVUT/Predmety/TestovaniSW/testsw1bendasta/selenium-1/geckodriver-v0.31.0-linux64/geckodriver");
        driver = new FirefoxDriver();
    }
    @Test
    public void flightFormTest(){
        driver.get("https://flight-order.herokuapp.com");
        System.out.println(driver.findElement(By.cssSelector("body > div > h1")).getAttribute("textContent"));
    }
}
