public class Horejvo1 {

    public long factorial (int n) {
        if (n == 0) {
            return 1;
        } else if(n >= 1){
            return n * factorial(n - 1);
        }
        return -1;
    }
}
