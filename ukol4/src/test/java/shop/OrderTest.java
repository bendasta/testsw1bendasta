package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderTest {
    int state = 1;
    ShoppingCart shoppingCart = new ShoppingCart();
    String customerName = "Dominik";
    String customerAddress = "Praha";
    int id = 1;
    String name = "Mirza";
    float price = 1.0F;
    String category = "People";
    int loyaltyPoints = 10;
    StandardItem standardItem = new StandardItem(id, name, price, category, loyaltyPoints);

    @Test
    public void Order_ShorterConstructorIsOK_GoodInstances(){
        shoppingCart.addItem(standardItem);
        Order order = new Order(shoppingCart, customerName, customerAddress);
        assertEquals(order.getCustomerName(), customerName);
        assertEquals(order.getCustomerAddress(), customerAddress);
        assertEquals(order.getItems().size(), 1);
        assertEquals(order.getItems().get(0), standardItem);
    }

    @Test
    public void Order_LongerConstructorIsOK_GoodInstances(){
        shoppingCart.addItem(standardItem);
        Order order = new Order(shoppingCart, customerName, customerAddress, state);
        assertEquals(order.getCustomerName(), customerName);
        assertEquals(order.getCustomerAddress(), customerAddress);
        assertEquals(order.getItems().size(), 1);
        assertEquals(order.getItems().get(0), standardItem);
        assertEquals(order.getState(), state);
    }

    @Test
    public void Order_ShoppingCartIsNotNull_NullPointerException(){
        Assertions.assertThrows(NullPointerException.class,
                () -> {
                    new Order(null, "Dominik", "Praha Loosova", 1);
                });
    }

}
