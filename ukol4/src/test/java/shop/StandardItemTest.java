package shop;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

public class StandardItemTest {
    int id;
    String name;
    float price;
    String category;
    int loyaltyPoints;
    StandardItem standardItem;
    StandardItem standardItemCopy;

    @BeforeEach
    public void setup(){
        id = 1;
        name = "Dominik";
        price = 8.5F;
        category = "People";
        loyaltyPoints = 88;
        standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        standardItemCopy = standardItem.copy();
    }

    @Test
    public void StandardItem_ConstructorIsOK_GoodInstances() {
        assertEquals(id, standardItem.getID());
        assertEquals(name, standardItem.getName());
        assertEquals(price, standardItem.getPrice());
        assertEquals(category, standardItem.getCategory());
        assertEquals(loyaltyPoints, standardItem.getLoyaltyPoints());
    }

    @Test
    public void copy_CopiesWell_CorrectCopy() {

        assertEquals(standardItemCopy.getID(), standardItem.getID());
        assertEquals(standardItemCopy.getName(), standardItem.getName());
        assertEquals(standardItemCopy.getPrice(), standardItem.getPrice());
        assertEquals(standardItemCopy.getCategory(), standardItem.getCategory());
        assertEquals(standardItemCopy.getLoyaltyPoints(), standardItem.getLoyaltyPoints());
    }

    @ParameterizedTest
    @CsvSource({"1, Mirza, 88, People, 44, 1, Mirza, 88, People, 44"})
    public void equals_ComparesObj1AndObj2_TheyAreEqual(int id, String name, float price, String category, int loyaltyPoints, int id2, String name2,
                                                        float price2,String category2, int loyaltyPoints2) {
        StandardItem item1 = new StandardItem(id, name, price, category, loyaltyPoints);
        StandardItem item2 = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);

        assertTrue(item1.equals(item2));
    }

}
