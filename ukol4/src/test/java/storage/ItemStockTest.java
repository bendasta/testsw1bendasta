package storage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.StandardItem;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemStockTest {

    int id;
    String name;
    float price;
    String category;
    int loyaltyPoints;
    StandardItem standardItem;
    ItemStock itemStock;

    @BeforeEach
    public void setup(){
        id = 1;
        name = "Bus";
        price = 2.5F;
        category = "Cars";
        loyaltyPoints = 100;
        standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        itemStock = new ItemStock(standardItem);
    }

    @Test
    public void ItemStock_ConstructorIsOK_GoodInstances() {
        assertEquals(0, itemStock.getCount());
        assertEquals(standardItem, itemStock.getItem());
    }

    @ParameterizedTest
    @CsvSource({"1,1", "-1,-1"})
    void IncreaseItemCount_WhenAItemsAreAdded_BEqualsThis(int incr, int result) {
        itemStock.IncreaseItemCount(incr);
        Assertions.assertEquals(result,itemStock.getCount());

    }

    @ParameterizedTest
    @CsvSource({"1,-1", "-1,1"})
    public void DecreaseItemCount_WhenIStartOnCountCAndITakeAway(int decr, int remain) {
        itemStock.decreaseItemCount(decr);
        assertEquals(remain, itemStock.getCount());
    }
}
