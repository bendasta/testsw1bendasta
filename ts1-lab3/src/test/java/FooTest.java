import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FooTest {
    //NazevMetody_TestovanyStav_OčekávanýVýstup
    Foo foo;

    @BeforeEach
    public void declareFoo() {
        foo = new Foo();
    }

    @Test
    public void ReturnNumber_ReturningSpecificNumber_Return5() {
        //ARRANGE
        int expectedValue = 5;

        //ACT
        int result = foo.returnNumber();

        //ASSERT
        assertEquals(expectedValue, result);
    }

    @Test
    public void GetNum_GetDefaultNumber_Return0() {
        //ARRANGE
        int expectedValue = 0;

        //ACT
        int result = foo.getNum();

        //ASSERT
        assertEquals(expectedValue, result);
    }

    @Test
    public void Increment_returnGreaterNumber_PlusOne() {
        //ARRANGE
        int expectedValue = foo.getNum();
        //ACT
        foo.increment();
        //ASSERT
        Assertions.assertTrue(expectedValue == foo.getNum());
    }

    @ParameterizedTest(name = "{0} plus {1} should be equal to {2}")
    @CsvFileSource(resources = "data.csv", numLinesToSkip = 1)
    public void add_addsAandB_returnsC(int a, int b, int c) {
        // arrange
        int expectedResult = c;
        // act
        int result = foo.sum(a, b);
        // assert
        assertEquals(expectedResult, result);
    }
}
