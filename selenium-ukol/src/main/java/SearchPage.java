import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchPage {
    private final WebDriver driver;
    @FindBy(id = "query")
    private WebElement searchBar;
    @FindBy(id = "search")
    private WebElement searchButton;


    public SearchPage(WebDriver driver) {
        this.driver = driver;
        if (this.driver.getCurrentUrl().equals("data:,")) {
            driver.get("https://link.springer.com/search");
        }
        PageFactory.initElements(driver, this);
    }

    public void insertWord(String searchWord) {
        this.searchBar.sendKeys(searchWord);
    }

    public void search() {
        this.searchButton.click();
    }

    public void chooseArticle() {
        driver.get(driver.getCurrentUrl() + "&facet-content-type=%22Article%22");
        //this.driver.findElement(By.xpath("//a[@href='/search?facet-content-type=%22Article%22']")).click();
    }

    public WebDriver getDriver() {
        return this.driver;
    }

    public void openArticle(int number) {
        String link = "";
        link = this.driver.findElement(By.xpath("//ol[@id='results-list']/li[" + number + "]/h2/a")).getAttribute("href");
        this.driver.get(link);
    }

}
