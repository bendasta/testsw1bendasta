import org.openqa.selenium.WebDriver;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Application {
    private WebDriver driver;
    private final List<String> informations;

    public Application(WebDriver driver, int number) throws InterruptedException, IOException {
        informations = new ArrayList<>();
        this.driver = driver;
        this.driver = mySearch(this.driver);
        Thread.sleep(2000);
        this.driver = articlesOnly(this.driver);
        String searchPage = this.driver.getCurrentUrl();
        this.driver = scanArticle(this.driver, number);
        writeIntoFile();
    }

    public WebDriver mySearch(WebDriver driver) throws InterruptedException {
        AdvancedSearchPage asp = new AdvancedSearchPage(driver);
        asp.setEverything("", "Page Object Model", "Sellenium Testing", "", "", "");
        asp.setTime("2022");
        Thread.sleep(1000);
        asp.search();
        return asp.getDriver();
    }

    public WebDriver articlesOnly(WebDriver driver) {
        SearchPage sp = new SearchPage(driver);
        sp.chooseArticle();
        return sp.getDriver();
        //https://link.springer.com/search?query=%22Page+Object+Model%22+AND+%28Sellenium+OR+Testing%29&date-facet-mode=in&facet-start-year=2022&showAll=true&facet-content-type=%22Article%22
        //https://link.springer.com/search?date-facet-mode=in&query=%22Page+Object+Model%22+AND+%28Sellenium+OR+Testing%29&facet-start-year=2022&showAll=true&facet-content-type=%22Article%22
    }

    public WebDriver scanArticle(WebDriver driver, int num) {
        SearchPage sp = new SearchPage(driver);
        sp.openArticle(num);
        ArticlePage ap = new ArticlePage(driver);
        informations.add(ap.getTitle());
        informations.add(ap.getTime());
        informations.add(ap.getDOI());
        return ap.getDriver();
    }

    public void writeIntoFile() throws IOException {
        FileWriter fw = new FileWriter("src/output/output.txt");
        for (String info : informations) {
            fw.write(info + "\n");
        }
        fw.close();
    }

}
