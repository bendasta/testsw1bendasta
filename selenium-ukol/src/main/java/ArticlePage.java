import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ArticlePage {
    private final WebDriver driver;
    @FindBy(xpath = "//h1[@class='c-article-title']")
    private WebElement nameOfArticle;
    @FindBy(xpath = "//a[@data-track-action='publication date']/time")
    private WebElement publicationTime;


    public ArticlePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getTitle() {
        return nameOfArticle.getText();
    }

    public String getTime() {
        return publicationTime.getText();
    }

    public String getDOI() {
        return driver.findElement(By.xpath("//li[@class='c-bibliographic-information__list-item c-bibliographic-information__list-item--doi']/p/span[@class='c-bibliographic-information__value']")).getText();
    }

    public WebDriver getDriver() {
        return this.driver;
    }

}
