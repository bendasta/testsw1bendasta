import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    private final WebDriver driver;
    @FindBy(id = "query")
    private WebElement searchBar;
    @FindBy(id = "search")
    private WebElement searchButton;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        driver.get("https://link.springer.com");
        PageFactory.initElements(driver, this);
    }

    public void signUp() {
        //this.driver.findElement(By.className("register-link flyout-caption")).click();
        this.driver.findElement(By.xpath("//a[@class='register-link flyout-caption']")).click();
    }

    public void insertWord(String searchWord) {
        this.searchBar.sendKeys(searchWord);
    }

    public void search() {
        this.searchButton.click();
    }

    public WebDriver getDriver() {
        return this.driver;
    }

}
